from flask import Blueprint

bp = Blueprint('api', __name__)

from app.api import document, docx, users, generate, generator, search, summarize, translate, translator, errors, tokens
