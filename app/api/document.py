from flask import jsonify, request, url_for
import json
from app import db
from app.models import User, Document
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request
from elasticsearch import Elasticsearch
es = Elasticsearch("35.196.62.192:9200")

@bp.route('/document',methods=['GET', 'POST'])
@bp.route('/opinion/<path:id>',methods=['GET', 'POST'])
@bp.route('/document/<path:id>',methods=['GET', 'POST'])
def get_document(id):
    #id = id.split("/")
    document = Document.query.get(id)
    jsondoc = json.loads(document.body)
    return jsonify(jsondoc)
    '''
    if len(id)==1:
        document=Document.query.get(id[0])#//psql query
        #document = es.get(index="document", doc_type='document',id=id[0])
    elif len(id)==3:
        document=es.search(index='document', body={'query': {
            'bool': {
                'must': {
                    'bool' : { 'must': [
                        { 'match': { 'body.docket.absolute_url': '/'+id[0]+'/'+id[1] }} ] }
                },
                'must_not': { 'match': {'authors': 'radu gheorge' }}
            }
        },'size': 1})
        document=document['hits']['hits'][0]['_source']
    return jsonify(document)
    '''


