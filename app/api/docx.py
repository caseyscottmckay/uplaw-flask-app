import os

from docx.shared import Inches, Pt, RGBColor
from flask import jsonify, request, url_for, current_app

from app.models import UserFormDataEntry
from app.api import bp
import docx
from docxtpl import DocxTemplate, Subdoc
from docx import Document
import json
import subprocess



#LOGO = "logo.png"    # the logo to include in each doc page
#CSV  = "data.csv"    # the file containing the csv data
from app.utils.utils import get_product


@bp.route('/docx', methods=['GET', 'POST'])
def get_docx(entry_id):
    referer_url = request.headers.get("Referer")
    preview = True
    if referer_url and '/' in referer_url:
        referer_page = referer_url.split("/")[3]
        if referer_page.startswith('checkout') or referer_page.startswith('my-account')or referer_page.startswith('my-account/downloads/'):
            preview = False
    if referer_url is None:
        referer_url ='none'
    slug = request.args.get("slug")
    product = get_product(slug)
    file_name = slug + "_" + str(entry_id)
    document_package = Document()
    if UserFormDataEntry.query.filter_by(id=entry_id).first() is None:
        user_form_data_entry =""
    else:
        user_form_data_entry = UserFormDataEntry.query.filter_by(id=entry_id).first().__dict__

    cover_page = make_docx_cover_page_from_template(product)
    instructional_guide = make_docx_guide_from_template(preview, product)

    cover_page_and_guide = combine_documents(cover_page, instructional_guide)
    standard_document = make_docx_from_template(file_name, slug, user_form_data_entry)
    document_package = combine_documents(cover_page_and_guide,standard_document)

    document_package.save(os.path.join(current_app.root_path,                                          current_app.config['PUBLIC_DOWNLOAD_FOLDER'])+file_name+".docx")

    doc = DocxTemplate(os.path.join(current_app.root_path,                                                  current_app.config['PUBLIC_DOWNLOAD_FOLDER'])+file_name+".docx")

    if not 'body' in user_form_data_entry:
        doc.render('')
    if 'body' in user_form_data_entry:
        body =json.loads(user_form_data_entry['body'])
        doc.render(body)
    doc.save(os.path.join(current_app.root_path,                                                  current_app.config['PUBLIC_DOWNLOAD_FOLDER'])+file_name+".docx")
    make_pdf_from_docx_template(file_name)
    return file_name


def combine_documents(doc1, doc2):
    for element in doc2.element.body:
        doc1.element.body.append(element)
    return doc1


def make_docx_from_template(file_name, slug, user_form_data_entry):
    if 'body' in user_form_data_entry:
        body = json.loads(user_form_data_entry['body'])
    doc = DocxTemplate(os.path.join(current_app.root_path, current_app.config['DOCX_TEMPLATE_FOLDER']+'documents/') + slug + ".docx")
   # doc.render(body)
    docx_file_name=  os.path.join(current_app.root_path, current_app.config['PUBLIC_DOWNLOAD_FOLDER']) + file_name + ".docx"
    if 'body' in user_form_data_entry:
        append_to_doc(doc,
                      employees_covered_under_a_collective_bargaining_agreement(file_name, user_form_data_entry))
        append_to_doc(doc, acknowledgment_of_receipt_and_review(file_name, user_form_data_entry))
    doc.add_page_break()
    doc.save(docx_file_name)
    return doc


def employees_covered_under_a_collective_bargaining_agreement(file_name, row):
    public_download_directory_absolute_path = os.path.join(current_app.root_path, current_app.config['PUBLIC_DOWNLOAD_FOLDER'])
    body = json.loads(row['body'])
    doc = Document()
    if 'employees_covered_under_a_collective_bargaining_agreement' in body and body['employees_covered_under_a_collective_bargaining_agreement'] == 'yes':
        append_to_doc(doc,
                      '/home/casey/uplaw/app/templates/docx/documents/employees-covered-under-a-collective-bargaining-agreement.docx')
    docx_file_name= public_download_directory_absolute_path + file_name + "_employees_covered_under_a_collective_bargaining_agreement.docx"
    doc.add_page_break()
    doc.save(docx_file_name)
    return docx_file_name

def acknowledgment_of_receipt_and_review(file_name, row):
    public_download_directory_absolute_path = os.path.join(current_app.root_path, current_app.config['PUBLIC_DOWNLOAD_FOLDER'])
    body = json.loads(row['body'])
    doc = Document()
    if 'acknowledgment_of_receipt_and_review' in body and body['acknowledgment_of_receipt_and_review'] == 'yes':
         append_to_doc(doc,
                      '/home/casey/uplaw/app/templates/docx/documents/acknowledgment-of-receipt-and-review.docx')
   # doc.render(body)
    docx_file_name= public_download_directory_absolute_path + file_name + "_acknowledgment_of_receipt_and_review.docx"
    doc.save(docx_file_name)

    return docx_file_name


def make_pdf_from_docx_template(file_name):
    cmd = ["/usr/bin/abiword", "--to=pdf", str(os.path.join(current_app.root_path,                                                  current_app.config['PUBLIC_DOWNLOAD_FOLDER']) + file_name + ".docx")]
    subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)

def make_docx_cover_page_from_template(product):
    doc = DocxTemplate(
        os.path.join(current_app.root_path, current_app.config['DOCX_TEMPLATE_FOLDER'] + 'documents/include/cover_page.docx'))

    doc.render(product)
    return doc

def make_docx_guide_from_template(preview, product):
    slug = product['slug']
    guide = os.path.join(current_app.root_path, current_app.config['DOCX_TEMPLATE_FOLDER'] + 'guides/' + slug + ".docx")
    if preview==False and os.path.isfile(guide):
        doc = DocxTemplate(guide)
        doc.render(product)
    if not os.path.isfile(guide) or preview==True:
        doc = DocxTemplate(
            os.path.join(current_app.root_path,
                         current_app.config['DOCX_TEMPLATE_FOLDER'] + 'guides/default_guide.docx'))
        doc.render(product)
    return doc


'''
def make_docx(file_name, row):
    body = json.loads(row['body'])
    document = Document()
    append_to_doc(document,  os.path.join(current_app.root_path,                                         current_app.config['DOCX_TEMPLATE_FOLDER'] + 'documents/') + "cover-page.docx")
    append_to_doc(document, "app/static/public/download/"+file_name+".docx")
    document.add_heading(file_name, 0)
    p = document.add_paragraph('A plain paragraph having some ')
    p.add_run('bold').bold = True
    p.add_run(' and some ')
    p.add_run('italic.').italic = True
    document.add_heading('Heading, level 1', level=1)
    document.add_paragraph('Intense quote', style='Intense Quote')
    #document.add_paragraph('name: ' + body['name'])
    document.add_paragraph(
        'first item in unordered list', style='List Bullet'
    )
    document.add_paragraph(
        'first item in ordered list', style='List Number'
    )
    # document.add_picture('monty-truth.png', width=Inches(1.25))
    records = (
        (3, '101', 'Spam'),
        (7, '422', 'Eggs'),
        (4, '631', 'Spam, spam, eggs, and spam')
    )
    table = document.add_table(rows=1, cols=3)
    hdr_cells = table.rows[0].cells
    hdr_cells[0].text = 'Qty'
    hdr_cells[1].text = 'Id'
    hdr_cells[2].text = 'Desc'
    for qty, id, desc in records:
        row_cells = table.add_row().cells
        row_cells[0].text = str(qty)
        row_cells[1].text = id
        row_cells[2].text = desc

    #t2 = Document("/home/casey/uplaw/app/static/public/download/workplace-searches-policy_17.docx")
    #for p in t2.paragraphs:
    #    document.add_paragraph(p.text, p.style)
    docx_file_name=os.path.join(current_app.root_path,                                                  current_app.config['PUBLIC_DOWNLOAD_FOLDER']) + file_name + "_.docx"
    document.save(docx_file_name)
    return docx_file_name
'''


def make_docx_cover_page(product):
    document = docx.Document()
    # logo = document_package.add_picture(LOGO, width=Inches(2.00))
    document.add_paragraph(product['title'], 'Title')
    p = document.add_paragraph()
    p.add_run(' \n ')
    p.add_run(product['jurisdiction']).bold = True
    p.add_run(' \n ')
    p.add_run(product['category']).bold = True
    document.add_paragraph(product['description'])
    document.add_heading('Document Package Includes:\n', level=1)

    document.add_paragraph(
        'Customized Final Document', style='List Bullet',
    )
    document.add_paragraph(
        'Flexible Platform Allows Updating Document Anytime', style='List Bullet',
    )
    document.add_paragraph(
        'Unlimited Downloads with Flexible Formatting (.pdf, .docx, .txt)', style='List Bullet',
    )
    document.add_paragraph(
        'Instructional Guide with Explanatory Notes', style='List Bullet',
    )
    document.add_paragraph(
        'UpLaw Law Library Pro Subscription (3 months)', style='List Bullet',
    )
    document.add_paragraph(
        'Lifetime Monitoring and Updates', style='List Bullet',
    )
    document.add_paragraph(
        'Money Back Guarantee', style='List Bullet',
    )
    document.add_page_break()
    return document





def getText(filename):
    doc = docx.Document(filename)
    fullText = []
    for para in doc.paragraphs:
        fullText.append(para.text)
    return '\n'.join(fullText)


def append_to_doc(doc,fname):
    t = Document(fname)
    for p in t.paragraphs:
        doc.add_paragraph("",p.style)       # add an empty paragraph in the matching style
        for r in p.runs:
            nr = doc.paragraphs[-1].add_run(r.text)
            nr.bold = r.bold
            nr.italic = r.italic
            nr.underline = r.underline
    doc.add_page_break()
    return doc
