from flask import jsonify, request, url_for, current_app, g
import json
from app import db
from app.models import User, Document
from app.api import bp, generator
from app.api.auth import token_auth
from app.api.errors import bad_request
from elasticsearch import Elasticsearch
es = Elasticsearch("35.196.62.192:9200")
#from app.api.generator import app
#import app.api.generator.app as a
@bp.route('/generate', methods=['GET', 'POST'])
def generate():
    generated_text = 'generated text'
    return jsonify(generated_text)