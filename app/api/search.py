from flask import jsonify, request, url_for, current_app, g
import json
from app import db
from app.models import User, Document
from app.api import bp
from app.api.auth import token_auth
from app.api.errors import bad_request
from elasticsearch import Elasticsearch
es = Elasticsearch("35.196.62.192:9200")

@bp.route('/search', methods=['GET', 'POST'])
def search():
    # if not g.search_form.validate():
    # return redirect(url_for('main.explore'))
    page = request.args.get('page', 1, type=int)
    query = request.args.get('q')
    if not g.search_form:
        query = request.args.get('q')
    if g.search_form:
        query = g.search_form.q.data
    if not query:
        query = '*'
    documents, aggregations, total, query = get_search_results('document', query, page,
                                                               current_app.config['POSTS_PER_PAGE'])
    next_url = url_for('main.search', q=query, page=page + 1) \
        if total > page * current_app.config['POSTS_PER_PAGE'] else None
    prev_url = url_for('main.search', q=query, page=page - 1) \
        if page > 1 else None

    return  jsonify(get_search_results('document', query, page,
                                                               current_app.config['POSTS_PER_PAGE']))

def get_search_results(index, query, page, per_page):
    if (request.args.get('document_type')):
        query = query + "AND body.document_type.keyword:" + request.args.get('document_type')
    if (request.args.get('jurisdiction')):
        query = query + "AND body.jurisdiction.keyword:" + request.args.get('jurisdiction')
    if (request.args.get('resource_type')):
        query = query + "AND body.resource_type.keyword:" + request.args.get('resource_type')
    if (request.args.get('state')):
        query = query + "AND body.state.keyword:" + request.args.get('state')
    if (request.args.get('jurisdiction_federal')):
        query = query + "AND body.state.keyword: federal"
    if (request.args.get('jurisdiction_state')):
        query = query + "AND NOT (body.state.keyword: federal)"
    if request.args.get('document_type_filter'):
        query = query + " AND body.document_type.keyword:" + request.args.get('document_type_filter')
    if request.args.get('resource_type_filter'):
        query = query + " AND body.resource_type.keyword:" + request.args.get('resource_type_filter')
    if request.args.get('jurisdiction_filter'):
        query = query + " AND body.jurisdiction.keyword:" + request.args.get('jurisdiction_filter')
    if request.args.get('date_filter'):
        query = query + " AND body.date.date:" + request.args.get('date_filter')
    if request.args.get('date_min_low') and request.args.get('date_min_high'):
        query = query + " AND body.date.date:[" + request.args.get('date_min_low') + " TO " + request.args.get(
            'date_min_high') + "]"
    if request.args.get('date_min_low') and not request.args.get('date_min_high'):
        query = query + " AND body.date.date:[" + request.args.get('date_min_low') + " TO 2019-12-31]"
    if request.args.get('date_min_high') and not request.args.get('date_min_low'):
        query = query + " AND body.date.date:[1492-01-01 TO " + request.args.get('date_min_high') + "]"
    if not query or query == "":
        query = '*'
        query = query + "AND body.state.keyword: federal"
    if request.args.get('sort_filter'):
        search = current_app.elasticsearch.search(index=index, body={"query": {
            "query_string": {
                "fields": ["body.title", "body.citation.citation", "body.jurisdiction","body.plain_text"],
                "query": query
            }
        },

            "_source": ["body.title", "body.date.date", "body.jurisdiction", "body.resource_type", "body.citation",
                        "body.word_count"],

            "aggregations": {"document_type_aggregations": {"terms": {"field": "body.document_type.keyword"}},
                             "jurisdiction_aggregations": {
                                 "terms": {"field": "body.jurisdiction.keyword", "size": 100}},
                             "resource_type_aggregations": {
                                 "terms": {"field": "body.resource_type.keyword", "size": 100}},
                             "date_aggregations": {"terms": {"field": "body.date.date"}}},
            "sort": [{"body.date.date": {"order": request.args.get('sort_filter')}}], 'from': (page - 1) * per_page,
            'size': per_page})
    else:
        search = current_app.elasticsearch.search(index=index, body={"query": {
            "query_string": {
                "fields": ["body.title", "body.citation.citation", "body.jurisdiction", "body.html"],
                "query": query
            }
        },

            "_source": ["body.title", "body.date.date", "body.jurisdiction", "body.resource_type", "body.citation",
                        "body.word_count", "body.html"],

            "aggregations": {"document_type_aggregations": {"terms": {"field": "body.document_type.keyword"}},
                             "jurisdiction_aggregations": {
                                 "terms": {"field": "body.jurisdiction.keyword", "size": 100}},
                             "resource_type_aggregations": {
                                 "terms": {"field": "body.resource_type.keyword", "size": 100}},
                             "date_aggregations": {"terms": {"field": "body.date.date"}}},

            "highlight": {
                "number_of_fragments": 3,
                "fragment_size": 150,
                "fields": {
                    "_all": {"pre_tags": ["<em>"], "post_tags": ["</em>"]},
                    # "body.title" : { "number_of_fragments" : 0 },
                    # "body.html": { "number_of_fragments" : 0 },
                    "body.html": {"number_of_fragments": 5, "order": "score"}
                }
            },

            'from': (page - 1) * per_page, 'size': per_page})

    # ids = [(hit['_source']['id']) for hit in search['hits']['hits']]
    search['hits']['hits']
    return search['hits']['hits'], search['aggregations'], search['hits']['total'], query
