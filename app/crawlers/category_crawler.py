import json
import re
from pprint import pprint

def get_categories_and_subcategories():
    with open('/data/uplaw/categories.txt') as f:
        content = f.readlines()
    categories_and_subcategories={}
    for line in content:
        if '--' in line:
            cats = line.split("--")
            for cat in cats[1:]:
                subcats = cat.split(",")
                category=subcats[0].strip()
                subcategories=[]
                for subcat in subcats[1:]:
                    subcategories.append(subcat.strip())
                categories_and_subcategories[category]=subcategories
    pprint(json.dumps(categories_and_subcategories))
    return categories_and_subcategories
get_categories_and_subcategories()

#lines = list(open('/home/casey/uplaw/data/categories.txt'))
#print(lines)