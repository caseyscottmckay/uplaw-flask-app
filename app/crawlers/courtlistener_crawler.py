import os
import glob
import hashlib
import psycopg2
import wget
import tarfile
import re, time
import requests, bs4, json
import utils

data_dumps_directory = '/home/casey/uplaw/data/courtlistener/'
urls = []
urls.append('https://www.courtlistener.com/api/bulk-data/opinions/all.tar')
urls.append('https://www.courtlistener.com/api/bulk-data/courts/all.tar.gz')
urls.append('https://www.courtlistener.com/api/bulk-data/educations/all.tar.gz')
urls.append('https://www.courtlistener.com/api/bulk-data/dockets/all.tar')
urls.append('https://www.courtlistener.com/api/bulk-data/clusters/all.tar')
urls.append('https://www.courtlistener.com/api/bulk-data/people/all.tar.gz')
urls.append('https://www.courtlistener.com/api/bulk-data/politicial-affiliations/all.tar.gz')


def getJurisdictions():
    res = requests.get("https://www.courtlistener.com/api/jurisdictions/")
    noStarchSoup = bs4.BeautifulSoup(res.text)
    elems = noStarchSoup.select("body > div.container.round-bottom > div.row.content > div > div > table > tbody > tr")
    objects = {}
    for row in elems:
        trs = row.select("td")
        jdxObject = {}
        jdxObject['name'] = (trs[0].getText().strip())
        jdxObject['jurisdiction'] = (trs[2].getText().strip())
        jdxObject['website'] = (trs[3].getText().strip())
        jdxObject['abbreviation'] = (trs[4].getText().strip())
        jdxObject['citation_abbreviation'] = (trs[5].getText().strip())
        jdxObject['start_date'] = (trs[6].getText().strip())
        jdxObject['end_date'] = (trs[7].getText().strip())
        jdxObject['in_use'] = (trs[8].getText().strip())
        objects[jdxObject['abbreviation']] = jdxObject
    #jdxs = json.dumps(objects).encode('utf8')
    jurisdiction_objects = json.loads(json.dumps(objects))
    return jurisdiction_objects

jurisdiciton_objects = getJurisdictions()

def processCourtListenerTarGZDirectory(filename):
    tar = tarfile.open(filename, "r:gz")
    sample_size = 0
    for member in tar.getmembers():
        #sample_size = sample_size + 1
        #if sample_size == 6:
        #    break
        print(member.name)
        f = tar.extractfile(member)
        content = f.read().decode('utf-8')
        body = content.replace('\x00', '')
        type = filename.split("/")[-2]
        pid = hashlib.md5(body.encode('utf-8')).hexdigest()
        body = json.loads(body)
        if type.endswith("s"):
            type = type[0:-1]
        if type == 'politicial-affiliation':
            type = 'politics'
        jdx = filename.split("/")[-1].split(".")[0]

        if jdx in jurisdiciton_objects:
            body['jurisdiction'] = jurisdiciton_objects[jdx]
        else:
            body['jurisdiction'] = jdx

        body['did'] = str(member.name).split('.json')[0]
        #body['type'] = type
        #url = type + '/' + body['did'] + "/" + body['slug']
        body = json.dumps(body)

        if re.compile('^.*?courtlistener/opinions.*?').match(filename):
            b, url = parce_courtlistener_opinion(body)
            body = json.dumps(b)
        utils.insert_into_elasticsearch(str(pid), body, type, url)
        #insert_document(str(pid), body, type, url)
    tar.close()



def parce_courtlistener_opinion(body):
    body = json.loads(body)
    newbody = {}
    newbody['html'] = ''
    if body['jurisdiction']['jurisdiction']:
        newbody['resource_type'] = body['jurisdiction']['name']
        newbody['jurisdiction'] = body['jurisdiction']['jurisdiction']
        newbody['court_abbreviation'] = body['jurisdiction']['abbreviation']
        newbody['citation_abbreviation'] = body['jurisdiction']['citation_abbreviation']
    elif body['jurisdiction']:
        newbody['jurisdiction'] = body['jurisdiction']
    newbody['did'] = body['did']
    newbody['text'] = ''
    #newbody['type'] = body['type']
    absolute_url = body['absolute_url']
    slug = absolute_url.split("/")[-2:-1]
    slug = slug[0]
    slug_title_case = str(slug).title().replace("-V-", "-v.-").replace("-a-", "-a-").replace("-The-", "-the-").replace(
        "-Of-", "-of-").replace("-Or-", "-or-").replace("-And-", "-and-").replace("-An-", "-an-")
    newbody['title'] = str(slug_title_case).replace("-", " ")
    url = ''
    if body['absolute_url']:
        url = body['absolute_url'][1:]
        newbody['url'] = url
    if body['resource_uri']:
        newbody['uri'] = body['resource_uri']
    if body['cluster']:
        newbody['cluster'] = body['cluster']
    if body['date_created']:
        newbody['date'] = body['date_created']
    if body['html_lawbox']:
        newbody['html'] = body['html_lawbox']
    if body['html_columbia']:
        newbody['html'] = body['html_columbia']
    if body['html']:
        newbody['html'] = body['html']
    if body['html_with_citations']:
        newbody['html'] = body['html_with_citations']
    newbody['text'] = utils.cleanhtml(newbody['html'])
    if body['plain_text']:
        newbody['text'] = body['plain_text']
    body = newbody
    return body, url


def download(urls):
    for url in urls:
        file_name = url.split("/")[-2]
        directory = os.path.join(data_dumps_directory, file_name)
        if not os.path.exists(directory):
            os.makedirs(directory)
        if len(os.listdir(directory)) == 0:
            print('Beginning file download with wget module' + directory)
            wget.download(url, directory)
            print(len(os.listdir(directory)))
            if len(os.listdir(directory)) > 0:
                for file in os.listdir(directory):
                    if file.endswith(".tar"):
                        print("extracting tar " + directory)
                        tar = tarfile.open(directory)
                        tar.extractall()
                        tar.close()
        else:
            print("Files already downloaded in " + directory)





#download(urls)


for subdir in os.listdir(data_dumps_directory):
    document_file_path = os.path.join(data_dumps_directory, subdir) + "/*gz"
    if 'opinion' in document_file_path:
        pattern = re.compile("^.*?.tar.gz$")
        for filepath in glob.iglob(document_file_path):
            if pattern.match(filepath):
                print("Processing: " + filepath)
                try:
                    processCourtListenerTarGZDirectory(filepath)
                except:
                    pass
            else:
                print("Not Processing: " + filepath)



#processCourtListenerTarGZDirectory('/home/casey/uplaw/data/courtlistener/opinions/scotus.tar.gz')
# processCourtListenerTarGZDirectory('/home/casey/uplaw/data/courtlistener/clusters/scotus.tar.gz')
# processCourtListenerTarGZDirectory('/home/casey/uplaw/data/courtlistener/dockets/scotus.tar.gz')
