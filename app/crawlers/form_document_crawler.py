import datetime
import hashlib
import json
import os
import re
import tarfile
import time

import psycopg2
from bs4 import BeautifulSoup
import utils


wp_product_import_header = 'post_title, post_name, short_description, description, categories, tags, regular_price, type, sold_individually, download_limit, download_expiry, Download 1 name, Download 1 url, sku, published, visibility, product_url, Purchase Note'


purchase_note = "Thank you for your business. Email support@uplaw.us if you need further assistance. Have a nice day."


def get_description(description, title):
    description_tail = 'This document package includes your customized legal document, an instructional guide with directions for using the document, a three-month UpLaw Law Library subscription to research your legal issues, and lifetime monitoring and updates.'
    if description is None:
        description = description_tail
    if not description is None:
        description = re.sub('incorporated into an employee handbook or used as a stand-alone policy document','used as a independent document or used in an employee handbook', description)
        description = re.sub('applies only to private workplaces', 'applies only to private workplaces in the United States',description)
        description = re.sub('It is jurisdiction neutral.','',description)
        description=re.sub('State or local law may impose additional or different requirements but this document will be useful and relevant to employers in every state.','',description)
        description = re.sub(
            "has integrated notes with .*?\.\s",
            description_tail, description)
        description = re.sub(r'([Aa]) sample', r'\1', description)
        description = re.sub(r'is a standard form', 'is a legal form',description)
        description = re.sub('[Ss]tandard [Dd]ocument', title, description)
        description = re.sub(' model ', ' legal ', description)
        description = re.sub('For information on.*?\.','',description)
        description = re.sub(r'^([A-Z](.*?)\.)\s.*?$',r'\1', description)
        description=description+" "+description_tail
    return description

def urlify(s):
    s = re.sub("r\'s","s",s)
    s = re.sub(r"[^\w\s]", '', s)
    s = re.sub(r"\s+", '-', s)
    s = s.lower()
    return s


def get_form_fields(content):
    fields = {}
    m = re.findall('\[[[^\[[\]]*\}', content)
    for i in m:
        name=i[2:len(i) - 1].strip()
        slug= re.sub("-","_",urlify(i)).strip()
        field = {}
        field['name'] = i[1:len(i) - 1].strip()
        field['slug'] = re.sub("-","_",urlify(i)).strip()
        if not i[1:len(i) - 1] in fields:
            fields[i[1:len(i) - 1].strip()] = field
    return fields


def clean_document(raw_html):
    parsed_html = BeautifulSoup(raw_html)
    doc_content_all = parsed_html.body.find('div', attrs={'id': 'co_docContentBody'}).text
    abstract = parsed_html.body.find('div', attrs={'class': 'co_contentBlock kh_abstract'}).text.strip()
    paragraphs = parsed_html.body.find_all('div', attrs={'class': 'co_paragraph'})

    cleantext = replace_text(re.compile('<div class="co_paragraphText">'), '<div class="co_paragraph_text>"', raw_html)
    raw_html = cleantext
    cleantext = replace_text(re.compile('<a.*?href=\"http.*?>'), '<a href=\"#\"">', raw_html)
    raw_html = cleantext

    cleantext = replace_text(re.compile('\['), '{{', raw_html)
    raw_html = cleantext
    cleantext = replace_text(re.compile('\]'), '}}', raw_html)
    '''
    raw_html=cleantext
    cleantext = replace_text(re.compile("class=\".*?\""),"",raw_html)
    raw_html=cleantext
    cleantext = replace_text(re.compile("style=\".*?\""),"",raw_html)
    '''
    # last clean
    # cleantext = replace_text(re.compile('<.*?>'), '', raw_html)
    fileout = open('/home/casey/uplaw/dump/is.txt' , 'w')
    #fileout.writelines(paragraphs)
    for paragraph in paragraphs:
        paragraph_text = paragraph.text
        paragraph_text = re.sub("[\n]{2,}","",paragraph_text)
        paragraph_text = replace_text(re.compile('\['), '{{', paragraph_text)
        paragraph_text = replace_text(re.compile('\]'), '}}', paragraph_text)

        fileout.write(paragraph_text)
    fileout.close()
    return cleantext


def replace_text(regex, replacement, text):
    cleantext = re.sub(regex, replacement, text)
    return cleantext


def insert_document_postgresql(host,table_name,pid, body, type, url):
    conn = None
    try:
        conn = psycopg2.connect(host=host, database="uplaw", user="postgres", password="postgres")
        # conn = sqlite3.connect('/home/casey/uplaw/app.db')
        cur = conn.cursor()
        # cur.execute("INSERT INTO document(pid,type,body) VALUES(%s,%s,%s) RETURNING id;", (var1, var2, var3))

        # cur.execute("INSERT INTO "+table_name+"(pid,type,body) VALUES(%s,%s,%s) RETURNING id;", (pid,type,body,))
        cur.execute("INSERT INTO " + table_name + "(pid,type,body,url,timestamp) VALUES (%s,%s,%s,%s,%s)",
                    (pid, type, body, url, int(round(time.time() * 1000)),))

        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()

#processTarGZDirectory("/home/casey/uplaw/data/standard_documents/standard_document.tar.gz")

#parse_form_document('/data/uplaw/standard_documents/standard_document/0051e6ca71b31651a70cec7d34441470')
def get_type(filename):
    parts = filename.split('--')
    type = None
    if len(parts) > 0:
        type = parts[0]
        if type == 'sd': return 'form'
        if type == 'gd': return 'guide'
        if type == 'ck': return 'checklist'
        if type == 'ar': return 'article'
    return type

def get_category(filename):
    parts = filename.split('--')
    category = 'Uncategorized'
    if len(parts) > 0:
        category=parts[2]
        if category == 'labor-and-employment': return 'Labor and Employment'
        elif category == 'corporate': return 'Corporate'
        elif category == 'commercial': return 'Commercial'
        elif category == 'intellectual-property-and-technology' or category == 'ip-and-technology': return 'Intellectual Property and Technology'
        elif category == 'estates': return 'Estates'
        else: category = re.sub('-'," ",category).title()
        if '-2' in category:
            category = re.sub("-[0-9]{1}","",category)
    return category

def get_subcategory(filename):
    parts = filename.split('--')
    subcategory = 'General'
    if len(parts) > 3:
        subcategory=parts[3]
        subcategory=re.sub("-[0-9]{1}", "", subcategory)
        if subcategory == 'it': return 'IT'
        elif subcategory == 'adr': return 'ADR'
        elif subcategory == 'commercial': return 'Commercial'
        elif subcategory == 'intellectual-property-and-technology' or subcategory == 'ip-and-technology': return 'Intellectual Property and Technology'
        elif subcategory == 'estates': return 'Estate'
        else: subcategory = re.sub('-'," ",subcategory).title()

    return make_title(subcategory)

def get_jurisdiction(filename):
    parts = filename.split('--')
    jurisdiction = 'International'
    if len(parts) > 0:
        jurisdiction=parts[1]
    if 'federal' in jurisdiction:
        jurisdiction="United States, Federal"
    if 'global' in jurisdiction:
        jurisdiction="International"
    if 'us' in jurisdiction:
        jurisdiction = "United States, National"
    jurisdiction = make_title(jurisdiction)
    return jurisdiction

csv_lines=[]
csv_lines.append(wp_product_import_header+"\n")
def parse_binders(file):
    print(file)
    content = ''

    if 'tar.gz' in file:
        content = file.read().decode('utf-8')
    elif not 'tar.gz' in file:
        f = open(file, 'r')
        content = f.read()
    documents = content.split("<html")
    category = get_category(file)
    subcategory = get_subcategory(file)
    type = get_type(file.split('/')[len(file.split('/'))-1])
    count =0
    for ogdocument in documents:
        count=count+1
        #if count >10:break
        document_json={}
        b={}
        notes_json={}
        html_lines=[]
        text_lines=[]
        raw_html ="<html"+ ogdocument
        #raw_html = re.sub('\[',"{{",raw_html)
        #raw_html = re.sub('\]',"}}",raw_html)
        parsed_html = BeautifulSoup(raw_html)
        title=parsed_html.title
        if not title is None:
            title = title.text
            title = title.split('|')[0].strip()
            slug = urlify(title)
            product_name = parsed_html.body.find('div', attrs={'class', 'co_productname'})
            description = parsed_html.body.find('div', attrs={'class', 'co_contentBlock kh_abstract'})
            jurisdiction = get_jurisdiction(file)
            jdx = parsed_html.body.find(id='co_docContentMetaInfoJurisdictions')
            if not jdx == None:

                for j in jdx.find_all('li'):
                    jurisdiction+=", "+j.text
            if 'UK, United Kingdom' in jurisdiction:
                jurisdiction=re.sub("UK, United Kingdom","United Kingdom",jurisdiction)
            if 'China, China' in jurisdiction:
                jurisdiction=re.sub("China, China","China",jurisdiction)
            topics = parsed_html.body.find('div',attrs={'class','co_topics'})
            if not topics is None:
                topics = topics.find_all('a')
            tags =jurisdiction+', '
            tags += category+", "
            tags += subcategory+", "
            if not topics is None:
                for tag in topics:
                    tags+=tag.text+", "
            tags = tags.strip()[0:-1]
            document = parsed_html.body.find('div', attrs={'id': 'co_document'})
            paragraphs = parsed_html.body.find_all('div', attrs={'class': 'co_paragraphText'})
            #for p in parsed_html.select('div[class=co_paragraphText]'):
                #newp = "<p>"+p.text+"</p>"
                #p.replace_with(newp)
                #if not p.string==None:
                    #print(p.string)
                #print(p.string.wrap(p.new_tag('p')))

            note_icons = parsed_html.body.find_all('div', attrs={'class', 'co_notesIcon'})
            notes = parsed_html.body.find_all('div', attrs={'class','co_notesContent'})
            identifiers = parsed_html.body.find_all('span', attrs={'class', 'kh_identifier'})
            clauses = parsed_html.body.find_all('div', attrs={'class', 'kh_clause'})
            note_headings=[]
            text = ''
            html = ''
            for o in note_icons:
                note_headings.append(o.text.split("Note:")[1].strip())
                o.clear()
            i =0
            for o in notes:
                heading=note_headings[i]
                i = i + 1
                notes_json[heading]=str(o)
                o.clear()
            for o in clauses:
                text+=o.text+"\n"
                html+=str(o)+"\n"
                html_lines.append(str(o))
                text_lines.append(o.text)
            b['title'] = make_title(title)
            b['slug'] = urlify(title)
            b['jurisdiction'] = jurisdiction
            current_date = str(datetime.datetime.utcnow())
            parts = current_date.split(" ")
            current_date=parts[0]+"T"+parts[1]
            parts = current_date.split(".")
            current_date= parts[0]+"Z".strip()

            b['date']=(current_date)
            b['resource_type'] = "Form Document"
            b['tags'] = tags
#            if not topics == None: b['tags'] = re.sub("Also Found In ","",topics.text)
            b['category'] = make_title(category)
            b['subcategory'] = make_title(subcategory)
            b['type'] = type
            if not description==None:
                b['description'] = description.text
            else:
                b['description']='Leading a Culture of Innovation.'
            if not document ==None: b['form_fields'] = get_form_fields(document.text)
            #b['notes']=notes_json
            b['text'] = text
            b['html'] = html
            b['text_lines'] = text_lines
            b['html_lines'] = html_lines
            b['original'] = str(document)
            b['did'] = hashlib.md5(slug.encode("utf-8")).hexdigest()
            url=type+"/"+b['did']+"/"+slug
            document_json['body']=b

            make_wc_csv(b)
            pid = hashlib.md5(json.dumps(b).encode("utf-8")).hexdigest()
            insert_document_postgresql('localhost','document', str(pid), json.dumps(b), type, url)
            #insert_document_postgresql('68.183.142.126','document', str(pid), json.dumps(b), type, url)
            utils.insert_into_elasticsearch(pid=pid, body=json.dumps(b), type=type, url=url)


def make_wc_csv(body):
    regular_price = str(int(len(body['text'])/150))
    sku=hashlib.md5(body['slug'].encode("utf-8")).hexdigest()
    short_description = "[gravityform id='1' title='false' description='false']"
    type_sold_indiviually_virtual_downloadable_download_limit_download_expires = '\"simple, downloadable\", yes, -1, -1'
    downloadable_files = "https://app.uplaw.us/download?slug=" + body['slug']

    csv_line = "\"" + body['title'] + "\"" + ", " + "\"" + body['slug'] + "\"" + ", " + "\"" + short_description + "\"" + ", " + "\"" + body['description'] + "\"" + ", \"" + body['category']+", "+body['category']+" > "+body['subcategory']+ "\", " + "\"" + body['tags'] + "\"" + ", " + regular_price + ", " + type_sold_indiviually_virtual_downloadable_download_limit_download_expires + ", Download, " + downloadable_files + ", " + sku + ", " + '0' + ", hidden, " + 'product/' + body['slug'] + ", \"" + purchase_note + "\""

    if body['type'] == 'form' and len(body['category']) < 199 and len(body['tags']) < 199 and len(
            regular_price) < 10:
        csv_lines.append(csv_line+"\n")



def make_title(title):
    temp_title = title.title()
    temp_title = re.sub(r' A ', r' a ', temp_title)
    temp_title = re.sub(r' An ', r' an ', temp_title)
    temp_title = re.sub(r' And ', r' and ', temp_title)
    temp_title = re.sub(r' For ', r' for ', temp_title)
    temp_title = re.sub(r' Of ', r' of ', temp_title)
    temp_title = re.sub(r' Or ', r' or ', temp_title)
    temp_title = re.sub(r' The ', r' the ', temp_title)
    temp_title = re.sub(r' To ', r' to ', temp_title)
    temp_title = re.sub(r'\'S', r'\'s', temp_title)
    return temp_title.strip()


i = 0;
for root, dirs, files in os.walk("/home/casey/uplaw/data/standard_documents/binders/"):
    path = root.split(os.sep)
    for file in files:
        if 'sd' in file and ( '-us-' in file or '-federal-' in file):
            i = i+1
            #if i >5: break
            parse_binders(root+file)

woo_csv_file = open('/home/casey/uplaw/dump/woo.csv','w', newline='')
woo_csv_file.writelines(csv_lines)
woo_csv_file.close()


#raw_html = re.sub('\[',"{{",raw_html)
#raw_html = re.sub('\]',"}}",raw_html)

