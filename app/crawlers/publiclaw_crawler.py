import datetime
import hashlib
import json
import os
import re
import zipfile
import utils

data_dumps_directory = '/home/casey/uplaw/data/law.resource.org/states/'

def parse_date(date_string):
    date_formatted = utils.get_current_date()
    p = re.compile("(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?).*?(\d{1,2}).*?(\d{4})")
    month_abbreviations =re.compile("(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)[\s\.]")
    if len(p.findall(date_string))>0:
        date_string = p.findall(date_string)[0][0] +' '+ p.findall(date_string)[0][1] +' '+ p.findall(date_string)[0][2]
    try:
        if month_abbreviations.findall(date_string):
            date_time_obj = datetime.datetime.strptime(date_string, '%b %d %Y')
        else:
            date_time_obj = datetime.datetime.strptime(date_string, '%B %d %Y')
        parts = str(date_time_obj).split(" ")
        if len(parts)==2:
            date_formatted=parts[0]+"T"+parts[1]+"Z"
    except:
        print("Error: ValueError: time data does not match format"+'--> date_string from xml: ' +date_string)
    return date_formatted

def format_html(document_json):
    formatted_html = ''
    lines = document_json['body']['html'].split("\n")
    for line in lines:
        formatted_html+="<p>"+line+"</p>"
    formatted_html = re.sub('<bold>', "<b>", formatted_html)
    formatted_html = re.sub('</bold>', "</b>", formatted_html)
    formatted_html = re.sub('<italic>',"<i>",formatted_html)
    formatted_html = re.sub('</italic>', "</i>", formatted_html)
    formatted_html = re.sub('<block_quote>', '<span class=\"blockquote\">', formatted_html)
    formatted_html = re.sub('</block_quote>', "</span>", formatted_html)

    formatted_html = re.sub('<footnote_body>', '<span class=\"footnote_body\">', formatted_html)
    formatted_html = re.sub('</footnote_body>', "</span>", formatted_html)


    formatted_html = re.sub('<page_number>', '<span class=\"page_number\">', formatted_html)
    formatted_html = re.sub('</page_number>', "</span>", formatted_html)
    '''
    formatted_html = re.sub('<italic>', "<span>", formatted_html)
    formatted_html = re.sub('</italic>', "</span>", formatted_html)

    formatted_html = re.sub('<italic>', "<span>", formatted_html)
    formatted_html = re.sub('</italic>', "</i>", formatted_html)

    formatted_html = re.sub('<italic>', "<i>", formatted_html)
    formatted_html = re.sub('</italic>', "</span>", formatted_html)

    formatted_html = re.sub('<italic>', "<i>", formatted_html)
    formatted_html = re.sub('</italic>', "</span>", formatted_html)
    
    '''

    footnote_numbers = re.compile(r"<footnote_number>.*?</footnote_number>", re.DOTALL)
    if len(footnote_numbers.findall(formatted_html)) > 0:
        for cr in footnote_numbers.findall(formatted_html):
            cr_citation = re.sub("<.*?>", "", cr)
            cr_citation = re.sub("\[", "", cr_citation)
            cr_citation = re.sub("\]", "", cr_citation)
            formatted_html = re.sub("<footnote_number>",
                                    '<p class=\"footnote_number\" id=\"' + cr_citation.strip() + '\">',
                                    formatted_html)
        formatted_html = re.sub('</footnote_number>', "</p>", formatted_html)


    footnote_references = re.compile(r"<footnote_reference>.*?</footnote_reference>", re.DOTALL)
    if len(footnote_references.findall(formatted_html)) > 0:
        for cr in footnote_references.findall(formatted_html):
            cr_citation = re.sub("<.*?>", "", cr)
            cr_citation = re.sub("\[","",cr_citation)
            cr_citation = re.sub("\]","",cr_citation)
            formatted_html = re.sub("<footnote_reference>",'<p class=\"footnote_reference\"> <a href=\"#'+cr_citation.strip()+'\">',formatted_html)
        formatted_html = re.sub('</footnote_reference>', "</a></p>", formatted_html)


    cross_references = re.compile(r"<cross_reference>.*?</cross_reference>", re.DOTALL)
    if len(cross_references.findall(formatted_html)) > 0:
        for cr in cross_references.findall(formatted_html):
            cr_citation = re.sub("<.*?>","",cr)
            formatted_html = re.sub(cr, '<span class=\"citation\" data=\"' + utils.urlify(cr_citation) + '\"><a href=\"' + utils.urlify(cr_citation) + '\">'+cr_citation+'</a></span>', formatted_html)
        #print(len(cross_references.findall(formatted_html)))
    return formatted_html

def parse_opinion(xml_string):
    json_out = {}
    reporter_caption = re.compile(r"<reporter_caption>.*?</reporter_caption>", re.DOTALL)
    if len(reporter_caption.findall(xml_string)) > 0:
        json_out['title'] = (re.sub("<.*?>", "", reporter_caption.findall(xml_string)[0]).strip())
        json_out['slug'] = (utils.urlify(json_out['title']))
    citation = re.compile(r"<citation>.*?</citation>", re.DOTALL)
    if len(citation.findall(xml_string)) > 0:
        json_out['citation'] = (re.sub("<.*?>", "", citation.findall(xml_string)[0]).strip())
    date = re.compile(r"<date>.*?</date>", re.DOTALL)
    if len(date.findall(xml_string)) > 0:
        json_out['date'] = (re.sub("<.*?>", "", date.findall(xml_string)[0]).strip())
    docket = re.compile(r"<docket>.*?</docket>", re.DOTALL)
    if len(docket.findall(xml_string)) > 0:
        json_out['docket'] = (re.sub("<.*?>", "", docket.findall(xml_string)[0]).strip())
    posture = re.compile(r"<posture>.*?</posture>", re.DOTALL)
    if len(posture.findall(xml_string)) > 0:
        json_out['posture'] = (re.sub("<.*?>", "", posture.findall(xml_string)[0]).strip())
    attorneys = re.compile(r"<attorneys>.*?</attorneys>", re.DOTALL)
    if len(attorneys.findall(xml_string)) > 0:
        json_out['attorneys'] = (re.sub("<.*?>", "", attorneys.findall(xml_string)[0]).strip())
    panel = re.compile(r"<panel>.*?</panel>", re.DOTALL)
    if len(panel.findall(xml_string)) > 0:
        json_out['panel'] = (re.sub("<.*?>", "", panel.findall(xml_string)[0]).strip())
    opinion_text = re.compile(r"<opinion_text>.*?</opinion_text>", re.DOTALL)
    if len(opinion_text.findall(xml_string)) > 0:
        json_out['html'] = (opinion_text.findall(xml_string)[0])
        json_out['text'] = (utils.cleanhtml(opinion_text.findall(xml_string)[0]))
    return json_out

def parse_rule(xml_string):
    json_out={}
    statute_heading = re.compile(r"<statute_heading>.*?</statute_heading>", re.DOTALL)
    if len(statute_heading.findall(xml_string)) > 0:
        json_out['title'] = (re.sub("<.*?>","",statute_heading.findall(xml_string)[0]).strip())
        json_out['slug'] = (utils.urlify(json_out['title']))
    citation = re.compile(r"<citation>.*?</citation>", re.DOTALL)
    if len(citation.findall(xml_string)) > 0:
        json_out['citation'] = (re.sub("<.*?>", "", citation.findall(xml_string)[0]).strip())
    date = re.compile(r"<date>.*?</date>", re.DOTALL)
    if len(date.findall(xml_string)) > 0:
        json_out['date'] = (re.sub("<.*?>", "", date.findall(xml_string)[0]).strip())
        json_out['date'] =str(json_out['date'])
    statute_text = re.compile(r"<statute_text>.*?</statute_text>", re.DOTALL)
    if len(statute_text.findall(xml_string))>0:
        json_out['html']=(statute_text.findall(xml_string)[0])
        json_out['text']=(utils.cleanhtml(statute_text.findall(xml_string)[0]))
    return json_out


def parse_statue(xml_string):
    pass


def processZip(zipfilepath):
    lines = []
    count = 0

    with zipfile.ZipFile(zipfilepath) as z:
        for filename in z.namelist():
            parts = zipfilepath.split("/")
            jurisdiction = 'national'
            type = 'document'
            resource_type = None
            if '/states/' in zipfilepath:
                jurisdiction = parts[6]
                #state = parts[6]
                resource_type =parts[7].split('.')[0]
            if '/federal/' in zipfilepath:
                jurisdiction =  'federal'
                resource_type =parts[5].split('.')[0]
            elastic_object = {}
            elastic_object['type']=type
            elastic_object['body'] = {}
            elastic_object['body']['slug']=filename
            elastic_object['body']['type'] = filename
            if not os.path.isdir(filename) and 'xml' in filename:
                count = count + 1
                #if count > 10: break
                with z.open(filename) as f:
                    did=filename.split('/')[-1]
                    did = re.sub('\.xml','',did)
                    did = did.strip()


                    for line in f:
                        try:
                            lines.append(line.decode("utf-8"))
                        except:
                            print(' decoding error')
                    xmlBody=(''.join(lines))
                    if 'opinion' in xmlBody.split("\n")[0]:
                        elastic_object['type']='opinion'
                        elastic_object['body'] = parse_opinion(xmlBody)

                    if 'rule' in xmlBody.split("\n")[0] or 'statute' in xmlBody.split("\n")[0]:
                        if 'rule' in xmlBody.split("\n")[0]: elastic_object['type']='rule'
                        if 'statute' in xmlBody.split("\n")[0]: elastic_object['type']='statute'
                        elastic_object['body'] = parse_rule(xmlBody)
                #elastic_object['body']['state'] = state

                elastic_object['body']['jurisdiction'] = jurisdiction
                elastic_object['body']['resource_type'] = resource_type
                if 'date' in elastic_object['body']:
                    elastic_object['body']['date'] = parse_date(elastic_object['body']['date'])
                else:
                    elastic_object['body']['date'] = utils.get_current_date()
                elastic_object['body']['did'] = did
                elastic_object['url']=elastic_object['type']+"/"+did+"/"+elastic_object['body']['slug']

                if 'html' in elastic_object['body']:
                    elastic_object['body']['html']=format_html(elastic_object)
                else:
                    elastic_object['body']['html']= None
                pid = hashlib.md5(json.dumps(elastic_object['body']).encode('utf-8')).hexdigest()
                utils.insert_into_elasticsearch(pid, json.dumps(elastic_object['body']), elastic_object['type'], elastic_object['url'])



def run_public_law_crawler(data_dumps_directory):
    for r, d, f in os.walk(data_dumps_directory):
        for file in f:
            if '/states/' in data_dumps_directory and 'law.zip' in file:
                temp_state_dir_name=file.split('.')[0]
                with zipfile.ZipFile(data_dumps_directory+file) as z:
                    for filename in z.namelist():

                        if not os.path.isdir(filename) and '.zip' in filename:
                            if not filename.split('/')[1] in excluded:
                                print("processing: " + filename)
                                processZip(data_dumps_directory+temp_state_dir_name+'/' + filename)
                            else:
                                print("excluding: "+filename)
            if '/federal/' in data_dumps_directory and not 'law.zip' in file:
                if not file in excluded:
                    print("processing: "+ file)
                    processZip(data_dumps_directory+file)
                else:
                    print("excluding: " + file)


excluded = []
excluded.append("united_states_bankruptcy_court.zip")
excluded.append("supreme_court_of_the_united_states.zip")
excluded.append("united_states_court_of_appeals_for_the_federal_circuit.zip")
excluded.append("united_states_court_of_appeals_for_the_district_of_columbia_circuit.zip")
excluded.append("united_states_court_of_appeals_for_the_first_circuit.zip")
excluded.append("united_states_court_of_appeals_for_the_second_circuit.zip")
excluded.append("united_states_court_of_appeals_for_the_third_circuit.zip")
excluded.append("united_states_court_of_appeals_for_the_fourth_circuit.zip")
excluded.append("united_states_court_of_appeals_for_the_fifth_circuit.zip")
excluded.append("united_states_court_of_appeals_for_the_sixth_circuit.zip")
excluded.append("united_states_court_of_appeals_for_the_seventh_circuit.zip")
excluded.append("united_states_court_of_appeals_for_the_eighth_circuit.zip")
excluded.append("united_states_court_of_appeals_for_the_ninth_circuit.zip")
excluded.append("united_states_court_of_appeals_for_the_tenth_circuit.zip")
excluded.append("united_states_court_of_appeals_for_the_eleventh_circuit.zip")
excluded.append("court_opinions.zip")


run_public_law_crawler(data_dumps_directory)


