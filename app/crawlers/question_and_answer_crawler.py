import datetime
import hashlib
import json
import os
import pprint
import re

from app.utils import utils
from app.utils.utils import cleanhtml, urlify

try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup


def parse_and_index_question_and_answers_to_elasticsearch():
    files = []
    lines = []
    for root, dirs, files in os.walk("../../data/standard_documents/question_and_answer/"):
        path = root.split(os.sep)
        for file in files:

            if ".html" in file and "question_and_answer" in root:
                file_name = root + "/" + file
                lns = ""
                with open(file_name) as f:
                    category = file_name.split("/")[-1:][0]
                    category = category[0:-5]
                    re.sub('.html', '', category).strip()
                    for line in f:
                        lines.append(line)
                        lns += line + "\n"
                    parsed_html = BeautifulSoup(lns)
                    doc_content_all = parsed_html.body.find('div', attrs={'id': 'co_docContentBody'}).text
                    start = lns.find("AnswerData")
                    end = lns.find("\"CompareType\": \"country\",")
                    qajson = lns[start:end].strip()
                    qajson = "{\n\"" + qajson[0:-1] + "\n}"
                    if len(qajson)<100:
                        break
                    qa_json = json.loads(qajson)
                    questions = qa_json['AnswerData']['questions']
                    i = 0
                    for question in questions:
                        i = i + 1
                        if i > 1:
                            break
                        body = {}
                        body['qid'] = question['id']
                        body['category'] = category
                        body['title'] = question['shortText']
                        body['html'] = question['longText']
                        body['text'] = question['shortText']
                        answers = question['answers']
                        answers_json_array = []

                        aid=0
                        html_string = category+"\n"+question['shortText']+"\n"
                        text_string = category+"\n"+question['shortText']+"\n"
                        for answer in answers:
                            aid = aid+1
                            answer_json = {}
                            answer_json['jurisdiction'] = answer['jurisdiction']
                            # answer_json['author']=answer['author']
                            answer_json['html'] = answer['value']
                            answer_json['text'] = cleanhtml(answer_json['html']).strip()
                            answer_json['date'] = answer['currencyDate']
                            answer_json['status'] = answer['currencyStatus']
                            answers_json_array.append(answer_json)

                            html_string +=answer_json['jurisdiction']+"\n"+answer_json['html']+"\n"
                            text_string += answer_json['jurisdiction']+"\n"+answer_json['text']+"\n"

                        body['text']=text_string
                        body['html']=html_string
                        body['answers'] = answers_json_array
                        current_date = str(datetime.datetime.utcnow())
                        parts = current_date.split(" ")
                        current_date = parts[0] + "T" + parts[1]
                        parts = current_date.split(".")
                        current_date = parts[0] + "Z".strip()
                        body['date']=current_date
                        body['slug'] = urlify(body['title'])
                        body['resource_type'] = 'Question and Answer'
                        body['did'] = hashlib.md5(body['slug'].encode('utf-8')).hexdigest()
                        pid = hashlib.md5(json.dumps(body).encode('utf-8')).hexdigest()

                        url = 'question/' + body['did']+"/"+body['slug']
                        type = "question"
                        table_name = "question"
                    #insert_document_postgresql(table_name, pid, json.dumps(body), type, url)

                    utils.insert_into_elasticsearch(pid,body=json.dumps(body),type=type,url=url)

def make_sivo_qa_intents_json():
    files = []
    lines = []
    qa_converstion = open('/home/casey/sivo/Bot/qa_conversation.json', 'r')
    qaconverstion = qa_converstion.read()
    qa_converstion.close()
    qaconverstion = json.loads(qaconverstion)
    for root, dirs, files in os.walk("/home/casey/uplaw/data/standard_documents/question_and_answer/"):
        path = root.split(os.sep)
        intents = []
        for qac in qaconverstion:
            intents.append(qac)
        for file in files:
            if ".html" in file and "question_and_answer" in root:
                file_name = root + "/" + file
                lns = ""
                with open(file_name) as f:
                    category = file_name.split("/")[-1:][0]
                    category = category[0:-5]
                    re.sub('.html', '', category).strip()
                    for line in f:
                        lines.append(line)
                        lns += line + "\n"
                    parsed_html = BeautifulSoup(lns)
                    doc_content_all = parsed_html.body.find('div', attrs={'id': 'co_docContentBody'}).text
                    start = lns.find("AnswerData")
                    end = lns.find("\"CompareType\": \"country\",")
                    qajson = lns[start:end].strip()
                    qajson = "{\n\"" + qajson[0:-1] + "\n}"
                    if len(qajson) < 100:
                        break
                    qa_json = json.loads(qajson)
                    questions = qa_json['AnswerData']['questions']
                    i = 0
                    for question in questions:
                        i = i + 1
                        if i > 1:    break
                        body = {}
                        body['qid'] = question['id']
                        body['category'] = category
                        body['html'] = question['longText']
                        body['text'] = question['shortText']
                        answers = question['answers']
                        answers_json_array = []
                        patterns = []
                        responses = []
                        for answer in answers:
                            answer_json = {}
                            answer_json['jurisdiction'] = answer['jurisdiction']
                            # answer_json['author']=answer['author']
                            answer_json['html'] = answer['value']
                            answer_json['text'] = cleanhtml(answer_json['html']).strip()
                            answer_json['date'] = answer['currencyDate']
                            answer_json['status'] = answer['currencyStatus']
                            answers_json_array.append(answer_json)

                            patterns.append("In " + answer_json['jurisdiction'] + " " + body['text'])
                            responses.append("In " + answer_json['jurisdiction'] + " " + answer_json['text'])

                        # SIVO
                        intent = {}
                        intent['tag'] = hashlib.md5(body['text'].encode('utf-8')).hexdigest()
                        intent['patterns'] = patterns
                        intent['responses'] = responses
                        intent['context_set'] = category
                        intents.append(intent)
                        print(intent)

                    sivo_json = {}
                    sivo_json['intents'] = intents
                    sivo_content_file = open('/home/casey/sivo/Bot/content.json', 'w')
                    sivo_content_file.write(json.dumps(sivo_json))
                    sivo_content_file.close()


parse_and_index_question_and_answers_to_elasticsearch()
#make_sivo_qa_intents_json()