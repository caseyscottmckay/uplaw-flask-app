import os
import zipfile

import lxml
import xml.etree.ElementTree as ET

import xmltodict, json


uscode_download_url = 'https://uscode.house.gov/download/releasepoints/us/pl/116/33/xml_uscAll@116-33.zip'
zipfilepath ='/home/casey/uplaw/data/uscode.house.gov/xml_uscAll@116-33.zip'


def processZip(zipfilepath):


    count = 0

    with zipfile.ZipFile(zipfilepath) as z:
        for filename in z.namelist():
            if not os.path.isdir(filename):
                lines = []
                count = count + 1
                if count>100:break
                with z.open(filename) as f:
                    for line in f:
                        try:
                            lines.append(line.decode("utf-8"))
                        except:
                            print(' decoding error')
                    xmlbody = ''.join(lines)
                    o = xmltodict.parse(xmlbody)
                    uscode_doc_json = json.loads(json.dumps(o))
                    title = uscode_doc_json['uscDoc']['meta']['dc:title']
                    print(title)
                    #if 'uscDoc' in uscode_doc_json and 'main' in uscode_doc_json['uscDoc']:
                        #usc_title_notes = uscode_doc_json['uscDoc']['main']['title']['notes']['note']
                        #for note in usc_title_notes:
                            #if 'heading' in note:
                             #   print(note['heading']['#text'])





processZip(zipfilepath=zipfilepath)
