import re

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter, HTMLConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
from io import BytesIO

def pdf_to_text(path):
    manager = PDFResourceManager()
    retstr = BytesIO()
    layout = LAParams(all_texts=True)
    device = TextConverter(manager, retstr, laparams=layout)
    filepath = open(path, 'rb')
    interpreter = PDFPageInterpreter(manager, device)

    for page in PDFPage.get_pages(filepath, check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()

    filepath.close()
    device.close()
    retstr.close()
    text = text.decode("utf-8")
    #print(text)
    #a = re.search('^TITLE [IXVC]+\.[A-Z,;:\s]+$',text)
    a = re.findall('TITLE [IXVC]+\.[A-Z,;:\s]+',text)
    #print(a if a is not None else 'Not found')

    title_parts = re.split(r'TITLE [IXVC]+\.',text)
    #title_parts=title_parts[14:]
    m = re.compile("TITLE [IXVC]+\.")
    g = m.search(text)
    if g:
        print(g.group())
    #for title in title_parts:
       # print(title)


if __name__ == "__main__":
    pdf_to_text("/home/casey/uplaw/data/uscourts.gov/cv_rules_eff._dec._1_2018_0.pdf")
    #pdf_to_text("/home/casey/uplaw/data/uscourts.gov/cr_rules_eff._dec._1_2018_0.pdf")

from io import StringIO

from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
import os
import sys, getopt


# converts pdf, returns its text content as a string
def convert(fname, pages=None):
    if not pages:
        pagenums = set()
    else:
        pagenums = set(pages)

    output = StringIO()
    manager = PDFResourceManager()
    converter = TextConverter(manager, output, laparams=LAParams())
    interpreter = PDFPageInterpreter(manager, converter)

    infile = open(fname, 'rb')
    for page in PDFPage.get_pages(infile, pagenums):
        interpreter.process_page(page)
    infile.close()
    converter.close()
    text = output.getvalue()
    output.close
    #print(text)
    ruleParts = text.split('^Rule [0-9]+\.\s')
    for rule in ruleParts:
        print(rule)
    return text

#convert("/home/casey/uplaw/data/uscourts.gov/cv_rules_eff._dec._1_2018_0.pdf")



import io

from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfpage import PDFPage


def extract_text_from_pdf(pdf_path):
    resource_manager = PDFResourceManager()
    fake_file_handle = io.StringIO()
    converter = TextConverter(resource_manager, fake_file_handle)
    page_interpreter = PDFPageInterpreter(resource_manager, converter)

    with open(pdf_path, 'rb') as fh:
        for page in PDFPage.get_pages(fh,
                                      caching=True,
                                      check_extractable=True):
            page_interpreter.process_page(page)

        text = fake_file_handle.getvalue()

    # close open handles
    converter.close()
    fake_file_handle.close()

    if text:
        return text


#if __name__ == '__main__':
#    print(extract_text_from_pdf('/home/casey/uplaw/data/uscourts.gov/cv_rules_eff._dec#._1_2018_0.pdf'))



