import hashlib
import json
import os
import re
import time
import datetime

import requests
from elasticsearch import Elasticsearch
from flask import current_app, request
import psycopg2
from woocommerce import API

def get_current_date():
    current_date = str(datetime.datetime.utcnow())
    parts = current_date.split(" ")
    current_date = parts[0] + "T" + parts[1]
    parts = current_date.split(".")
    current_date = parts[0] + "Z".strip()
    return current_date

es = Elasticsearch(host='35.184.16.52')
def insert_into_elasticsearch(pid, body, type, url):
    temp = {}
    temp['body'] = json.loads(body)
    temp['type'] = type
    temp['url'] = url
    temp['pid'] = pid
    es.index(index="document", doc_type='document', body=temp)

def check_for_docx(slug):
    docx_template_directory_absolute_path = os.path.join(current_app.root_path, current_app.config[
        'DOCX_TEMPLATE_FOLDER'] + 'documents/' + slug + ".docx")
    if not os.path.isfile(docx_template_directory_absolute_path):
        return 0;
    elif os.path.isfile(docx_template_directory_absolute_path):
        return 1;


def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext

'''
def get_product(slug):
    url = 'form/' + slug
    product=None
    if slug and Document.query.filter_by(url=url).order_by(Document.id.desc()).first():
        row = Document.query.filter_by(url=url).order_by(Document.id.desc()).first().__dict__
        product = json.loads(json.dumps(row.get('body')))
    return product
'''

def get_user_form_data_entry(entry_id):
    slug = request.args.get('slug')
    url = "https://uplaw.us/wp-json/gf/v2/entries/"+str(entry_id)
    querystring = {"oauth_consumer_key": "ck_94bfe252b2bfaedb5fcccacf7125b5b9835c77b6",
                   "oauth_signature_method": "HMAC-SHA1", "oauth_timestamp": "1561300526", "oauth_nonce": "JJCL91",
                   "oauth_version": "1.0", "oauth_signature": "y6fH6dtuXMksQ2cZTNIL7Pxh Jk="}
    headers = {
        'authorization': "Basic Y2tfOTRiZmUyNTJiMmJmYWVkYjVmY2NjYWNmNzEyNWI1Yjk4MzVjNzdiNjpjc182YjFlYmQ1ZTlkOTYwYjc5ZGNjZjI3YmNhZWNlZmYxZDg4OWFmMmUz",
        'cache-control': "no-cache",
        'postman-token': "7b0c537d-7bbf-89e1-ab45-b60e5d5a96f5"
    }
    response = requests.request("GET", url, headers=headers, params=querystring)

    entry = response
    form_id =response.json()['form_id']
    url = "https://uplaw.us/wp-json/gf/v2/forms/"+str(form_id)
    querystring = {"oauth_consumer_key": "ck_94bfe252b2bfaedb5fcccacf7125b5b9835c77b6",
                   "oauth_signature_method": "HMAC-SHA1", "oauth_timestamp": "1561300526", "oauth_nonce": "JJCL91",
                   "oauth_version": "1.0", "oauth_signature": "y6fH6dtuXMksQ2cZTNIL7Pxh Jk="}
    headers = {
        'authorization': "Basic Y2tfOTRiZmUyNTJiMmJmYWVkYjVmY2NjYWNmNzEyNWI1Yjk4MzVjNzdiNjpjc182YjFlYmQ1ZTlkOTYwYjc5ZGNjZjI3YmNhZWNlZmYxZDg4OWFmMmUz",
        'cache-control': "no-cache",
        'postman-token': "7b0c537d-7bbf-89e1-ab45-b60e5d5a96f5"
    }
    response = requests.request("GET", url, headers=headers, params=querystring)
    form = response
    form_data={}
    for field in form.json()['fields']:
        if field['inputs']:
            for input in field['inputs']:
                if entry.json()[str(input['id'])]:
                    form_data[input['label']]=entry.json()[str(input['id'])]
        if str(field['id']) in entry.json():
            form_data[field['label']] =entry.json()[str(field['id'])]
    entry_json=entry.json()
    entry_json['form_data']=form_data
    user_form_data = {}
    #user_form_data['entry_id'] = entry_id
    user_form_data['slug'] = slug
    user_form_data['entry']=entry_json
    return user_form_data


def insert_document_postgresql(table_name,pid, body, type, url):
    conn = None
    try:
        conn = psycopg2.connect(host='localhost', database="uplaw", user="postgres", password="postgres")
        # conn = sqlite3.connect('/home/casey/uplaw/app.db')
        cur = conn.cursor()
        # cur.execute("INSERT INTO document(pid,type,body) VALUES(%s,%s,%s) RETURNING id;", (var1, var2, var3))

        # cur.execute("INSERT INTO "+table_name+"(pid,type,body) VALUES(%s,%s,%s) RETURNING id;", (pid,type,body,))
        cur.execute("INSERT INTO " + table_name + "(pid,type,body,url,timestamp) VALUES (%s,%s,%s,%s,%s)",
                    (pid, type, body, url, int(round(time.time() * 1000)),))

        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()


def get_gravity_form():
    #entry_id = request.args.get("entry_id")
    entry_id = str(1)
    response = requests.get('https://uplaw.us/wp-json/gf/v2/form/' + entry_id,
                            'ck_94bfe252b2bfaedb5fcccacf7125b5b9835c77b6', auth=(
        'ck_94bfe252b2bfaedb5fcccacf7125b5b9835c77b6', 'cs_6b1ebd5e9d960b79dccf27bcaeceff1d889af2e3'))
    #response = requests.get('https://uplaw.us/wp-json/gf/v2/entries/' + entry_id +'?_labels=1','ck_94bfe252b2bfaedb5fcccacf7125b5b9835c77b6',auth=('ck_94bfe252b2bfaedb5fcccacf7125b5b9835c77b6', 'cs_6b1ebd5e9d960b79dccf27bcaeceff1d889af2e3'))
    json_response = (json.loads(response.content))
    print(json_response)
    #labels = json_response['_labels']
    #print(labels)
    '''
    for key in json_response:
        print(key)
        if isinstance(json_response[key], str) and key in labels:
            print(key + ", " + labels[key] + ", " + json_response[key])

        if isinstance(json_response[key], dict):
            for k, v in json_response[key].items():
                print(k, ' ', v)
        # if key in labels:
        # print(key+", "+labels[key]+", "+json_response[key])
    # example usage
    return json_response
    '''


def get_wp_product(slug):
    wcapi = API(
        url="https://uplaw.us",
        consumer_key="ck_b0624601867f6642906608c487832a3a21d03105",
        consumer_secret="cs_3ae46a0b29793afd58ed2542b1187c041a9d83f8",
        version="wc/v3"
    )
    path = "products/?slug=" + slug
    r = wcapi.get(path)
    product = r.json()[0]
    product_json ={}
    product_json['id'] = product['id']
    product_json['slug'] = product['slug']
    product_json['name'] = product['name']
    #if len(product['downloads']) == 0:
        #product_json['download_name'] = -1
        #product_json['download_file'] = -1
    if len(product['downloads'])>0:
        product_json['download_name'] = product['downloads'][0]['name']
        product_json['download_file'] = product['downloads'][0]['file']
    product_json['price'] = product['price']
    product_json['categories'] = product['categories']
    product_json['tags'] = product['tags']
    product_json['short_description'] = product['short_description']
    return product_json


def get_wp_order(slug):
    wcapi = API(
        url="https://uplaw.us",
        consumer_key="ck_b0624601867f6642906608c487832a3a21d03105",
        consumer_secret="cs_3ae46a0b29793afd58ed2542b1187c041a9d83f8",
        version="wc/v3"
    )
    path = "products/?slug=" + slug
    r = wcapi.get(path)
    print(wcapi.get("orders").json())
    print(wcapi.get("orders/28225").json())
    '''
    product = r.json()[0]
    product_json ={}
    product_json['id'] = product['id']
    product_json['slug'] = product['slug']
    product_json['name'] = product['name']
    #if len(product['downloads']) == 0:
        #product_json['download_name'] = -1
        #product_json['download_file'] = -1
    if len(product['downloads'])>0:
        product_json['download_name'] = product['downloads'][0]['name']
        product_json['download_file'] = product['downloads'][0]['file']
    product_json['price'] = product['price']
    product_json['categories'] = product['categories']
    product_json['tags'] = product['tags']
    product_json['short_description'] = product['short_description']
    return product_json
    '''
    return ''


def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def urlify(s):
    s = re.sub("r\'s","s",s)
    s = re.sub(r"[^\w\s]", '', s)
    s = re.sub(r"\s+", '-', s)
    s = s.lower()
    return s

def md5(fname):
    hash_md5 = hashlib.md5()
    with open(fname, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def cleanhtml(raw_html):
    cleanr = re.compile('<.*?>')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


def insert_document_into_postgres(pid, body, type, url):
    conn = None
    try:
        conn = psycopg2.connect(host='localhost', database="uplaw", user="postgres", password="postgres")
        # conn = sqlite3.connect('/home/casey/uplaw/app.db')
        cur = conn.cursor()
        # cur.execute("INSERT INTO document(pid,type,body) VALUES(%s,%s,%s) RETURNING id;", (var1, var2, var3))
        table_name = type
        # cur.execute("INSERT INTO "+table_name+"(pid,type,body) VALUES(%s,%s,%s) RETURNING id;", (pid,type,body,))
        cur.execute("INSERT INTO " + table_name + "(pid,type,body,url,timestamp) VALUES (%s,%s,%s,%s,%s)",
                    (pid, type, body, url, int(round(time.time() * 1000)),))

        if type == 'opinion':
            table_name = 'document'
            cur.execute("INSERT INTO " + table_name + "(pid,type,body,url,timestamp) VALUES (%s,%s,%s,%s,%s)",
                        (pid, type, body, url, int(round(time.time() * 1000)),))


        conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()