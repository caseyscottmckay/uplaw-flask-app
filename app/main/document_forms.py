import time
from datetime import datetime

from flask import request, redirect
from flask_wtf import FlaskForm, widgets
from wtforms import StringField, SubmitField, TextAreaField, RadioField, BooleanField, validators, FileField, \
    SelectField, IntegerField, TextField, FormField, DateField, TimeField, HiddenField, DateTimeField, FieldList
from wtforms.validators import ValidationError, DataRequired, Length, Required, Optional, AnyOf, EqualTo
from flask_babel import _, lazy_gettext as _l
from app.models import User


class CollectivateBargainingAgreement():
    collective_bargaining_agreement = SelectField(u'Are employees under a collective bargaining agreement?', choices=[('yes', 'Yes'), ('no', 'No')])
    controlling_agreement = SelectField(u'Programming Language', choices=[('a', 'Employer\'s Agreement controls.'), ('b', 'Collective Bargaining Agreement controls.'), ('c', 'Plain Text')])


class DefaultProductRegistration(FlaskForm):
    docx_preview=HiddenField(default='False')
    email = StringField(_l('Email'), validators=[DataRequired()])
    submit = SubmitField('Submit')

class DmcaComplaint(FlaskForm):
    date = StringField("Date",default=datetime.today, validators=[validators.DataRequired()], description='Date refers to the effective date of this DMCA Complaint, which is generally the day you send it to the recipient.')
    method_of_delivery = SelectField('Method of Delivery', validators=[Optional()],choices=[('mail', 'mail'), ('email', 'email'), ('fax', 'fax')],description='DMCA Complaints are usually sent by email to the Service Provider.',default="email")
    service_provider = StringField('Name of service provider.', validators=[Optional()],description='A service providder can be a website providing, an ISP, or any other service provider infringing on your copyrights.',default="{{SERVICE PROVIDER}}")
    designated_agent = StringField('Designated Agent', validators=[Optional()], default="{{DESIGNATED AGENT}}", description="The DMCA safe harbor requires a service provider to: (1) Designate an individual as its copyright agent. (2) Make the agent's name and contact information (including mailing address, telephone number and e-mail address) publicly available on the service provider's website. (3) Register this information with the US Copyright Office.")
    designated_address = TextAreaField(u'Designated Mailing Address', [validators.optional(), validators.length(max=200)], default="{{DESIGNATED ADDRESS}}")
    designated_phone = StringField(_l('Designated Phone'), validators=[Optional()], default="{{DESIGNATED PHONE}}")
    designated_email = StringField(_l('Designated Email'), validators=[Optional()], default="{{DESIGNATED EMAIL}}" )
    copyright_ownwer = StringField('Name of copyright owner.', validators=[Optional()], default="{{COPYRIGHT OWNER}}")
    owner_author_publisher = SelectField('Copyright owners relationship to the copyrighted works at issue.', validators=[Optional()],choices=[('owner', 'owner'), ('author', 'author'),('publisher', 'publisher')], default='owner')
    description_of_copyrighted_work = TextAreaField(_l('Describe the copyrighted works at issue.'), validators=[Optional()], default='[DESCRIPTION OF THE COPYRIGHTED WORK]')
    senders_name = StringField('Sender\'s Name', validators=[Optional()], default="{{SENDER'S NAME}}")
    senders_address = TextAreaField(u'Sender\'s Mailing Address', [validators.optional(), validators.length(max=200)], default="{{SENDER'S ADDRESS}}")
    senders_phone = StringField(_l('Sender\'s Phone'), validators=[Optional()], default="{{SENDER'S PHONE}}")
    senders_email = StringField(_l('Sender\'s Email'), validators=[Optional()], default="{{SENDER'S EMAIL}}")
    title_of_work = StringField(_l('Title of work.'), validators=[Optional()], default="{{TITLE OF COPYRIGHTED WORK}}")

    copyright_registration_number = StringField(_l('Copyright registration number.'), default="{{COPYRIGHT REGISTRATION NUMBER}}")
    service_providers_service_url = StringField(_l('Service Provider\'s service url.'), default="{{SERVICE PROVIDER'S URL}}")
    displays_provides_access_to_caches = SelectField('How is the work presnted on the infringing site?', validators=[Optional()],choices=[('displayed', 'displays'), ('provides access to', 'provides access to'),('caches', 'caches')], default="{{DESCRIBE THE INFRINGING CONTENT}}")
    description_of_infringing_material = StringField('Describe the infringing material.',description="e.g., Website page hosting an infringing photograph." )
    url_of_infringing_material = StringField('What is the URL of the infringing content you described in the previous question.',
                                                     description="e.g., https://www.infringing-url.com/infringing-photo.jpg")

    terms_of_use_copyright_policy_code_of_conduct = SelectField('What copyright policy does the service provider maintain?', validators=[Optional()],choices=[('Terms of Use', 'Terms of Use'), ('Copyright Policy', 'Copyright Policy'),('Code of Conduct', 'Code of Conduct')], default="Terms of Use")
    submit = SubmitField('Submit')

class IMForm(FlaskForm):
    protocol = SelectField(choices=[('aim', 'AIM'), ('msn', 'MSN')])
    username = StringField()

class BackgroundCheckPolicy(FlaskForm):
    employer_name = StringField('What is the employer\'s name?', validators=[Required()])
    department_name = StringField('What department is responsible for the administration of this policy?', validators=[Required()], default="Human Resources",description="The Human Resources Department is often in charge of administering employment policies and procedures.")
    employees_covered_under_a_collective_bargaining_agreement = SelectField('Are employees under a collective bargaining agreement?',
                                                  choices=[('yes', 'yes'), ('no', 'no')], default='yes')
    name_of_policy = HiddenField(default="Workplace Searches Policy")
    position_allowed_to_change_policy = StringField('What position is allowed to make changes to this policy?', default='Manager', description="The Manager is often allowed to make changes to policies.")
    acknowledgment_of_receipt_and_review = SelectField('Include an Acknowledgment of Receipt and Review for each agreeing employee to sign?',choices=[('yes', 'yes'), ('no', 'no')], description="A signed acknowledgment of receipt, review, and understanding of any employee policy minimizes the potential for employees to later claim ignorance of that policy as an excuse for non-compliance.")
    at_will_employee = SelectField('Is the employee agreeing to this policy an \'at will\' employee?', choices=[('yes', 'yes'), (
        'no', 'no')], default='yes')
    submit = SubmitField('Submit')

class WorkplaceSearchesPolicy(FlaskForm):
    employer_name = StringField('What is the employer\'s name?', validators=[Required()])
    department_name = StringField('What department is responsible for the administration of this policy?', validators=[Required()], default="Human Resources",description="The Human Resources Department is often in charge of administering employment policies and procedures.")

    #locations_with_a_reasonable_expectation_of_privacy = StringField('In addition to restrooms, locker rooms, and hotel rooms, what other locations have a reasonable expectation of privacy?', validators=[Optional()])
    # = SelectField(u'Are employees under a collective bargaining agreement?',
    refusal_to_allow_search_or_inspection_may_result_in_discipline = SelectField("Refusal to allow search or inspection may result in discipline?",choices=[('yes','yes'), ('no','no')])
    employees_covered_under_a_collective_bargaining_agreement = SelectField('Are employees under a collective bargaining agreement?',
                                                  choices=[('yes', 'yes'), ('no', 'no')], default='yes')

    name_of_policy = HiddenField(default="Workplace Searches Policy")
    position_allowed_to_change_policy = StringField('What position is allowed to make changes to this policy?', default='Manager', description="The Manager is often allowed to make changes to policies.")
    acknowledgment_of_receipt_and_review = SelectField('Include an Acknowledgment of Receipt and Review for each agreeing employee to sign?',choices=[('yes', 'yes'), ('no', 'no')], description="A signed acknowledgment of receipt, review, and understanding of any employee policy minimizes the potential for employees to later claim ignorance of that policy as an excuse for non-compliance.")
    at_will_employee = SelectField('Is the employee agreeing to this policy an \'at will\' employee?', choices=[('yes', 'yes'), (
        'no', 'no')], default='yes')
    submit = SubmitField('Submit')


class NepotismPolicy(FlaskForm):
    employer_name = StringField('What is the employer\'s name?', validators=[Required()])

    '''
    employer_name = StringField('What is the employer\'s name?', validators=[Required(),AnyOf('aaa')])

    hr_certification = RadioField('HR Cert required', choices=[('yes', 'Yes, hr certification required.'), ('no', 'No, hr certification is not required.')])
    q2 = RadioField(
        "Are employees covered under a collective bargaining agreement or union?.",
        choices=[('yes', 'Yes.'), ('no', 'No.')])

    q3 = StringField(_l('Employer Email'), validators=[DataRequired()])
    q4 = TextAreaField(_l('Say something'), validators=[DataRequired()])
    message = TextAreaField(_l('Message'), validators=[
    DataRequired(), Length(min=1, max=140)])
    boolean_field_example = BooleanField('Remember Me')
    upload_field_example = FileField(u'Image File')
    language = SelectField(u'Programming Language', choices=[('cpp', 'C++'), ('py', 'Python'), ('text', 'Plain Text')])
    office_phone = FormField(TelephoneForm)
    magazine_form = FormField(MagazineIssueForm)

    start_date = DateField('Start date')
    start_time = TimeField('Start time')
    construction_year = IntegerField('construction_year')
    transfer_tax_rate = SelectField('transfer_tax_rate', validators=[Optional()],choices=[('ZERO', '0%'), ('SIX', '6%')])
    '''
    submit = SubmitField('Submit')



