import os
from datetime import datetime
import docx
import requests
from docxtpl import DocxTemplate
from elasticsearch_dsl.query import MultiMatch, Q
from flask import Flask, render_template, flash, Markup, redirect, url_for, g, \
    jsonify, current_app, request
from flask_login import current_user, login_required
import json
from flask_babel import _, get_locale
from googletrans import Translator
import string
import operator
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
import numpy as np
from sklearn.externals import joblib
import re
import subprocess
from elasticsearch_dsl import Search
from app import db
from app.api.docx import get_docx
from app.main.forms import EditProfileForm, SearchForm, MessageForm
from app.main.document_forms import NepotismPolicy, DmcaComplaint, WorkplaceSearchesPolicy, DefaultProductRegistration
from app.models import User, Post, Message, Notification, Document, UserFormDataEntry
from app.translate import translate
from app.main import bp
from app.utils.utils import check_for_docx, get_wp_product, get_product, get_user_form_data_entry


@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()
    g.locale = str(get_locale())


@bp.route('/user/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    page = request.args.get('page', 1, type=int)
    posts = user.posts.order_by(Post.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.user', username=user.username,
                       page=posts.next_num) if posts.has_next else None
    prev_url = url_for('main.user', username=user.username,
                       page=posts.prev_num) if posts.has_prev else None
    return render_template('user.html', user=user, posts=posts.items,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/user/<username>/popup')
@login_required
def user_popup(username):
    user = User.query.filter_by(username=username).first_or_404()
    return render_template('user_popup.html', user=user)


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash(_('Your changes have been saved.'))
        return redirect(url_for('main.edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title=_('Edit Profile'),
                           form=form)


@bp.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash(_('User %(username)s not found.', username=username))
        return redirect(url_for('main.index'))
    if user == current_user:
        flash(_('You cannot follow yourself!'))
        return redirect(url_for('main.user', username=username))
    current_user.follow(user)
    db.session.commit()
    flash(_('You are following %(username)s!', username=username))
    return redirect(url_for('main.user', username=username))


@bp.route('/unfollow/<username>')
# @login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash(_('User %(username)s not found.', username=username))
        return redirect(url_for('main.index'))
    if user == current_user:
        flash(_('You cannot unfollow yourself!'))
        return redirect(url_for('main.user', username=username))
    current_user.unfollow(user)
    db.session.commit()
    flash(_('You are not following %(username)s.', username=username))
    return redirect(url_for('main.user', username=username))


@bp.route('/translate', methods=['POST'])
@login_required
def translate_text():
    return jsonify({'text': translate(request.form['text'],
                                      request.form['source_language'],
                                      request.form['dest_language'])})


@bp.route('/about')
def about():
    title = "About"
    return render_template("about.html", title=title)

@bp.route('/ask')
def ask():
    return redirect("https://ask.uplaw.us")

@bp.route('/blog')
def blog():
    title = "Blawg"
    response = requests.get('https://uplaw.us/wp-json/wp/v2/posts')
    # response = requests.get('https://uplaw.us/wp-json/gf/v2/entries/' + entry_id +'?_labels=1','ck_94bfe252b2bfaedb5fcccacf7125b5b9835c77b6',auth=('ck_94bfe252b2bfaedb5fcccacf7125b5b9835c77b6', 'cs_6b1ebd5e9d960b79dccf27bcaeceff1d889af2e3'))
    #json_response = (json.loads(response.content))
    #return redirect("https://uplaw.us/blog")

    return render_template("blog.html", title=title, response=response.json())

@bp.route('/clients')
def clients():
    title = "Clients"
    return render_template("clients.html", title=title)


@bp.route('/contact')
def contact():
    title = "Contact"
    return render_template("contact.html", title=title)


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
def index():
    return render_template('index.html', title=_('Home'))


@bp.route('/services')
def services():
    title = "Services"
    return render_template("services.html", title=title)



POSTS_PER_PAGE = 20


@bp.route('/search', methods=['GET', 'POST'])
# @login_required
def search():
    q = request.args.get('q')
    page = request.args.get('page', 1, type=int)
    documents, total, aggregations = searchES(q, page, POSTS_PER_PAGE, request)
    next_url = url_for('main.search', q=q, page=page + 1) \
        if total > page * POSTS_PER_PAGE else None
    prev_url = url_for('main.search', q=q, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title=_('Search'), aggregations=aggregations, documents=documents,
                           total=total, next_url=next_url, prev_url=prev_url, q=q)


def searchES(query, page, per_page, request):
    from_ = (page - 1) * per_page
    size_ = per_page
    if query == '':
        query = 'case law'
    q = Q('match', type="opinion")
    #q = q | Q('match', type="form")
    #q = q | Q('match', type="question")
    q = q | Q('match', body__text=query)
    q = q | Q('match', body__title=query)
    if request.args.get('jurisdiction_federal'):
        q = q | Q('match', body__jurisdiction="Federal")
    if request.args.get('jurisdiction_state'):
        q = q | Q('match', body__jurisdiction="State")
    if request.args.get('state'):
        q = q | Q('match', body__resource_type=request.args.get('state'))
    s = Search(using=current_app.elasticsearch, index="document") \
        .query(q) \
        .highlight('body.title', 'body.text', fragment_size=50)
    if request.args.get('date_min_high') and request.args.get('date_min_low'):
        s = s.filter('range',
                     body__date={'from': request.args.get('date_min_low'), 'to': request.args.get('date_min_high')})
    elif request.args.get('date_min_high'):
        s = s.filter('range', body__date={'from': 1776, 'to': request.args.get('date_min_high')})
    elif request.args.get('date_min_low'):
        s = s.filter('range', body__date={'from': request.args.get('date_min_low'), 'to': datetime.utcnow()})

    if request.args.get('resource_types_filter'):
        s = s.filter('term', **{'body.resource_type.keyword': request.args.get('resource_types_filter')})
    if request.args.get('types_filter'):
        s = s.filter('term', **{'type.keyword': request.args.get('types_filter')})
    if request.args.get('dates_filter'):
        s = s.filter('term', **{'body.date': request.args.get('dates_filter')})
    if request.args.get('jurisdictions_filter'):
        s = s.filter('term', **{'body.jurisdiction.keyword': request.args.get('jurisdictions_filter')})
    # aggregations
    aggregations = {}
    s.aggs.bucket('types', 'terms', field='type.keyword')
    s.aggs.bucket('jurisdictions', 'terms', field='body.jurisdiction.keyword')
    s.aggs.bucket('resource_types', 'terms', field='body.resource_type.keyword')
    s.aggs.bucket('dates', 'terms', field='body.date')
    response = s.execute()
    aggregations['types'] = response.aggs.types.buckets
    aggregations['resource_types'] = response.aggs.resource_types.buckets
    aggregations['dates'] = response.aggs.dates.buckets
    aggregations['jurisdictions'] = response.aggs.jurisdictions.buckets
    # sorting
    if not request.args.get('sort_filter') is None:
        if request.args.get('sort_filter'):
            s = s.sort({"body.date": {"order": request.args.get('sort_filter'), "mode": "avg"}})
    # from and size
    s = s[from_:]

    return s, s.count(), aggregations


@bp.route('/send_message/<recipient>', methods=['GET', 'POST'])
@login_required
def send_message(recipient):
    user = User.query.filter_by(username=recipient).first_or_404()
    form = MessageForm()
    if form.validate_on_submit():
        msg = Message(author=current_user, recipient=user,
                      body=form.message.data)
        db.session.add(msg)
        user.add_notification('unread_message_count', user.new_messages())
        db.session.commit()
        flash(_('Your message has been sent.'))
        return redirect(url_for('main.user', username=recipient))
    return render_template('send_message.html', title=_('Send Message'),
                           form=form, recipient=recipient)


@bp.route('/messages')
@login_required
def messages():
    current_user.last_message_read_time = datetime.utcnow()
    current_user.add_notification('unread_message_count', 0)
    db.session.commit()
    page = request.args.get('page', 1, type=int)
    messages = current_user.messages_received.order_by(
        Message.timestamp.desc()).paginate(
        page, current_app.config['POSTS_PER_PAGE'], False)
    next_url = url_for('main.messages', page=messages.next_num) \
        if messages.has_next else None
    prev_url = url_for('main.messages', page=messages.prev_num) \
        if messages.has_prev else None
    return render_template('messages.html', messages=messages.items, next_url=next_url, prev_url=prev_url)


@bp.route('/export_posts')
@login_required
def export_posts():
    if current_user.get_task_in_progress('export_posts'):
        flash(_('An export task is currently in progress'))
    else:
        current_user.launch_task('export_posts', _('Exporting posts...'))
        db.session.commit()
    return redirect(url_for('main.user', username=current_user.username))


@bp.route('/notifications')
@login_required
def notifications():
    since = request.args.get('since', 0.0, type=float)
    notifications = current_user.notifications.filter(
        Notification.timestamp > since).order_by(Notification.timestamp.asc())
    return jsonify([{
        'name': n.name,
        'data': n.get_data(),
        'timestamp': n.timestamp
    } for n in notifications])

@bp.route('/opinion/<path:id>', methods=['GET', 'POST'])
@bp.route('/document/<path:id>', methods=['GET', 'POST'])
#@login_required
def document(id):

    if '/' in id:
        document = current_app.elasticsearch.search(index='document', body={"query": {
            "bool": {
                "must": {
                    "bool": {
                        "must": [
                            {"match": {"url": id}}

                        ]
                    }
                }
            }
        }
        })['hits']['hits'][0]
    else:
        document = current_app.elasticsearch.get(index="document", doc_type='document', id=id)
    type=(document['_source']['type'])
    if type == 'form' or type == 'question':
        return render_template('document.html', restricted=True)
    # document = id
    summary = id
    statistics = id
    translation = None
    text = document['_source']['body']['text']
    text = os.linesep.join([s for s in text.splitlines() if s])
    text = re.sub("\s\s+", " ", text)
    statistics = get_document_statistics(text)
    summary = summarize(text, 1)
    summary = str(summary[2])[2:-2]
    return render_template('document.html', document=document, summary=summary, statistics=statistics)


@bp.route('/registration', methods=['POST', 'GET'])
def registration():
    slug = request.args.get('slug')
    if '/' in slug:
        slug = slug.split('/')
        slug = slug[4]
    product = get_product(slug)

    return render_template("registration.html", product=product)






@bp.route('/consultation', methods=['POST', 'GET'])
def consultation():
    slug = request.args.get('slug')
    update = request.args.get('update')
    product = get_product(slug)
    form = get_document_form(slug)
    file_name = slug + "_0"
    if check_for_docx(slug) == 0:
        return redirect(url_for('main.checkout', slug=slug))
    if form.validate_on_submit():
        user_form_data_json = json.dumps(form.data)
        user_form_data = UserFormDataEntry(body=user_form_data_json,
                                           user_id=current_user.get_id(), slug=slug)
        db.session.add(user_form_data)
        db.session.commit()
        row = UserFormDataEntry.query.filter_by(slug=slug).order_by(
            UserFormDataEntry.id.desc()).first().__dict__
        entry_id = str(row.get('id'))

        get_docx(entry_id)
        if update == 'true':
            return redirect(url_for('main.download', entry_id=entry_id, slug=slug))
        return redirect(url_for('main.confirmation', entry_id=entry_id, slug=slug))
    return render_template("consultation.html", form=form, product_pdf_path=current_app.config['PUBLIC_DOWNLOAD_FOLDER'] + file_name + '.pdf')


@bp.route('/confirmation', methods=['POST', 'GET'])
def confirmation():
    entry_id = str(request.args.get('entry_id'))
    slug = str(request.args.get('slug'))
    file_name=slug+"_"+entry_id
    user_form_data_entry = get_user_form_data_entry(entry_id)
    make_docx_package(entry_id)
    product = get_wp_product(slug)
    product_pdf_path = current_app.config['PUBLIC_DOWNLOAD_FOLDER'] + file_name + '.pdf'
    return render_template("confirmation.html",user_form_data_entry=user_form_data_entry, product=product, product_pdf_path=product_pdf_path)


def make_docx_package(entry_id):
    user_form_data_entry = get_user_form_data_entry(entry_id)
    slug = user_form_data_entry['slug']
    print(user_form_data_entry['entry']['form_data'])
    #generate_document_template(slug)
    doc = DocxTemplate(
        os.path.join(current_app.root_path, current_app.config['DOCX_TEMPLATE_FOLDER'] + 'documents/') + slug + ".docx")
    doc.render(user_form_data_entry['entry']['form_data'])
    file_name=str(slug + "_" + entry_id)
    docx_file_name = os.path.join(current_app.root_path,
                                  current_app.config['PUBLIC_DOWNLOAD_FOLDER']) + file_name + ".docx"

    doc.add_page_break()
    doc.save(docx_file_name)
    cmd = ["/usr/bin/abiword", "--to=pdf",
           str(os.path.join(current_app.root_path, current_app.config['PUBLIC_DOWNLOAD_FOLDER']) + file_name + ".docx")]
    subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    return doc

def generate_document_template(slug):
    psql_document = Document.query.filter_by(url=str('form/'+slug)).first().__dict__

    document = docx.Document()
    document.add_heading('Document Heading', 0)
    p = document.add_paragraph('A plain paragraph having some ')
    p.add_run('bold').bold = True
    p.add_run(' and some ')
    p.add_run('italic.').italic = True
    document.add_heading('Heading, level 1', level=1)
    document.add_paragraph('Intense quote', style='Intense Quote')
    #document.add_paragraph('name: ' + body['name'])
    for line in psql_document['body']['text_lines']:
        document.add_paragraph(line)
    document.add_paragraph(
        'first item in unordered list', style='List Bullet'
    )
    document.add_paragraph(
        'first item in ordered list', style='List Number'
    )
    # document.add_picture('monty-truth.png', width=Inches(1.25))
    records = (
        (3, '101', 'Spam'),
        (7, '422', 'Eggs'),
        (4, '631', 'Spam, spam, eggs, and spam')
    )
    table = document.add_table(rows=1, cols=3)
    hdr_cells = table.rows[0].cells
    hdr_cells[0].text = 'Qty'
    hdr_cells[1].text = 'Id'
    hdr_cells[2].text = 'Desc'
    for qty, id, desc in records:
        row_cells = table.add_row().cells
        row_cells[0].text = str(qty)
        row_cells[1].text = id
        row_cells[2].text = desc

    #t2 = Document("/home/casey/uplaw/app/static/public/download/workplace-searches-policy_17.docx")
    #for p in t2.paragraphs:
    #    document.add_paragraph(p.text, p.style)
    docx_file_name=os.path.join(current_app.root_path, current_app.config['DOCX_TEMPLATE_FOLDER'] + 'documents/') + slug + ".docx"
    document.save(docx_file_name)
    return docx_file_name


@bp.route('/confirm', methods=['POST', 'GET'])
def confirm():
    slug = request.args.get("slug")
    wp_product = get_wp_product(slug)
    product = get_product(slug)
    product_id = str(wp_product['id'])
    entry_id = request.args.get("entry_id")
    row = UserFormDataEntry.query.filter_by(id=request.args.get("entry_id")).first().__dict__
    body = json.loads(row.get('body'))
    file_name = slug + "_" + entry_id;
    product_pdf_path = current_app.config['PUBLIC_DOWNLOAD_FOLDER'] + file_name + '.pdf'
    product_docx_path = current_app.config['PUBLIC_DOWNLOAD_FOLDER'] + file_name + '.docx'
    return render_template("confirm.html", body=body, slug=slug, product_id=product_id,
                           product_pdf_path=product_pdf_path, product_docx_path=product_docx_path, product=product)


@bp.route('/checkout', methods=['POST', 'GET'])
def checkout():
    product = get_wp_product(request.args.get('slug'))
    product_id = str(product['id'])
    uplaw_checkout_url = "https://uplaw.us/checkout?add-to-cart=" + product_id
    return redirect(uplaw_checkout_url)


@bp.route('/account', methods=['POST', 'GET'])
@bp.route('/account/<path:filename>', methods=['POST', 'GET'])
def account():
    return redirect("https://uplaw.us/my-account")

@bp.route('/team')
def team():
    return render_template("team.html")


@bp.route('/download', methods=['POST', 'GET'])
@bp.route('/download/<path:filename>', methods=['POST', 'GET'])
def download():
    user_id = current_user.get_id()
    if not request.args.get('slug') or request.args.get('slug') == '':
        return redirect(url_for("main.index"))
    slug = request.args.get("slug")
    product = get_product(slug)
    if check_for_docx(slug=slug) is 1:

        row = UserFormDataEntry.query.filter_by(slug=slug, user_id=user_id).order_by(
            UserFormDataEntry.id.desc()).first().__dict__
        body = json.loads(row.get('body'))
        entry_id = str(row.get('id'))
        file_name = slug + "_" + entry_id;
        product_pdf_path = current_app.config['PUBLIC_DOWNLOAD_FOLDER'] + file_name + '.pdf'
        product_docx_path = current_app.config['PUBLIC_DOWNLOAD_FOLDER'] + file_name + '.docx'
        get_docx(entry_id)
        return render_template("download.html", body=body, row=row, slug=slug, entry_id=entry_id, product=product,
                               product_docx_path=product_docx_path, product_pdf_path=product_pdf_path)
    elif check_for_docx(slug=slug) is 0:
        download_na_message = "Your document is not complete, so your download is not yet available. We are researching your issues and drafting your document to prepare your document package. \nMost services take 2-5 business day.\nYou will receive an email notification when your document is finished and available for download."
        return render_template("download.html", product=product, download_na_message=download_na_message)


# ToDo: Move Summarizers and Predictors to own directory

class summarizer:
    def get_summary(self, input, max_sentences):
        sentences_original = sent_tokenize(input)
        # Remove all tabs, and new lines
        if (max_sentences > len(sentences_original)):
            print("Error, number of requested sentences exceeds number of sentences inputted")
        # Should implement error schema to alert user.
        s = input.strip('\t\n')
        # Remove punctuation, tabs, new lines, and lowercase all words, then tokenize using words and sentences
        words_chopped = word_tokenize(s.lower())
        sentences_chopped = sent_tokenize(s.lower())
        stop_words = set(stopwords.words("english"))
        punc = set(string.punctuation)
        # Remove all stop words and punctuation from word list.
        filtered_words = []
        for w in words_chopped:
            if w not in stop_words and w not in punc:
                filtered_words.append(w)
        total_words = len(filtered_words)
        # Determine the frequency of each filtered word and add the word and its frequency to a dictionary (key - word,value - frequency of that word)
        word_frequency = {}
        output_sentence = []
        for w in filtered_words:
            if w in word_frequency.keys():
                word_frequency[w] += 1.0  # increment the value: frequency
            else:
                word_frequency[w] = 1.0  # add the word to dictionary
        # Weighted frequency values - Assign weight to each word according to frequency and total words filtered from input:
        for word in word_frequency:
            word_frequency[word] = (word_frequency[word] / total_words)
        # Keep a tracker for the most frequent words that appear in each sentence and add the sum of their weighted frequency values.
        # Note: Each tracker index corresponds to each original sentence.
        tracker = [0.0] * len(sentences_original)
        for i in range(0, len(sentences_original)):
            for j in word_frequency:
                if j in sentences_original[i]:
                    tracker[i] += word_frequency[j]
        # Get the highest weighted sentence and its index from the tracker. We take those and output the associated sentences.
        for i in range(0, len(tracker)):
            # Extract the index with the highest weighted frequency from tracker
            index, value = max(enumerate(tracker), key=operator.itemgetter(1))
            if (len(output_sentence) + 1 <= max_sentences) and (sentences_original[index] not in output_sentence):
                output_sentence.append(sentences_original[index])
            if len(output_sentence) > max_sentences:
                break
            tracker.remove(tracker[index])
        sorted_output_sent = self.sort_sentences(sentences_original, output_sentence)
        return (sorted_output_sent)

    def sort_sentences(self, original, output):
        sorted_sent_arr = []
        sorted_output = []
        for i in range(0, len(output)):
            if (output[i] in original):
                sorted_sent_arr.append(original.index(output[i]))
        sorted_sent_arr = sorted(sorted_sent_arr)
        for i in range(0, len(sorted_sent_arr)):
            sorted_output.append(original[sorted_sent_arr[i]])
        print(sorted_sent_arr)
        return sorted_output


@bp.route('/summarize', methods=['POST'])
def summarize(text, num_sentences):
    title = "Summarizer"
    max_value = sent_tokenize(text)
    num_sent = num_sentences
    sum1 = summarizer()
    summary = sum1.get_summary(text, num_sent)
    return title, text, summary, max_value


'''
from flask import Flask, render_template, request
import app.neuralnet.predict as pr
from console_logging.console import Console
console = Console()

@bp.route('/predict', methods=['POST'])
def predict():

    # get form variables and type them
    gpa = float(request.form["gpa"])
    score = int(request.form["test_score"])
    console.info("Chancing GPA: %d, SAT: %d"%(gpa,score))
    predictions=[]

    #TODO: implement test type. This is a stub.
    if score<=36:
        predictions=pr.predict(gpa,score,"ACT")
    elif score<=1600:
        predictions=pr.predict(gpa,score,"SAT1600")
    else:
        predictions = pr.predict(gpa,score,"SAT2400")
    ##

    if predictions[0]==1:
        return "Positive."
    else:
        if predictions[0]==0:
            return "Negative"
        return "Something went wrong."

@bp.route('/ai/predict')
def home():
    return render_template('predict.html', college={'name':'CMU','accuracy':'78.6517'})
'''


@bp.route("/predict", methods=['GET'])
def predict_price():
    return render_template("predict.html")


@bp.route("/scrape", methods=['GET'])
def scrape():
    listings = mongo.db.listings
    listings_data = scrape_craigslist.scrape()
    listings.update(
        {},
        listings_data,
        upsert=True
    )
    return redirect("/", code=302)


@bp.route('/get-user-data', methods=['POST'])
def predict_stuff():
    if request.method == 'POST':
        model = joblib.load('app/predict-price/trained_house_classifier_model.pkl')

        print('-----line 27--------')
        print(request.form.get('year_built'))

        year_built = int(request.form.get('year_built'))

        print('line 31')

        stories = int(request.form.get('stories'))
        num_bedrooms = int(request.form.get('num_bedrooms'))
        full_bathrooms = int(request.form.get('full_bathrooms'))
        half_bathrooms = int(request.form.get('half_bathrooms'))
        livable_sqft = int(request.form.get('livable_sqft'))
        total_sqft = int(request.form.get('total_sqft'))
        garage_sqft = int(request.form.get('garage_sqft'))

        carport_sqft = int(request.form.get('carport_sqft'))
        has_fireplace = request.form.get('has_fireplace')

        has_pool = request.form.get('has_pool')

        has_central_heating = request.form.get('has_central_heating')

        has_central_cooling = request.form.get('has_central_cooling')
        has_fireplace = request.form.get('has_fireplace')
        garage_type = request.form.get('garage_type')
        city = request.form.get('city')
        house_to_value = [
            # House features
            year_built,  # year_built
            stories,  # stories
            num_bedrooms,  # num_bedrooms
            full_bathrooms,  # full_bathrooms
            half_bathrooms,  # half_bathrooms
            livable_sqft,  # livable_sqft
            total_sqft,  # total_sqft
            garage_sqft,  # garage_sqft
            carport_sqft,  # carport_sqft
            1 if (has_fireplace == 'on') else 0,  # has_fireplace
            1 if (has_pool == 'on') else 0,  # has_pool
            1 if (has_central_heating == 'on') else 0,  # has_central_heating
            1 if (has_central_cooling == 'on') else 0,  # has_central_cooling

            # Garage type: Choose only one
            1 if (garage_type == 'attached') else 0,  # attached
            1 if (garage_type == 'detached') else 0,  # detached
            1 if (garage_type == 'none') else 0,  # none

            # City: Choose only one
            1 if (city == 'Amystad') else 0,  # Amystad
            1 if (city == 'Brownport') else 0,  # Brownport
            1 if (city == 'Chadstad') else 0,  # Chadstad
            1 if (city == 'Clarkberg') else 0,  # Clarkberg
            1 if (city == 'Coletown') else 0,  # Coletown
            1 if (city == 'Davidfort') else 0,  # Davidfort
            1 if (city == 'Davidtown') else 0,  # Davidtown
            0,  # East Amychester
            0,  # East Janiceville
            0,  # East Justin
            0,  # East Lucas
            0,  # Fosterberg
            0,  # Hallfort
            0,  # Jeffreyhaven
            0,  # Jenniferberg
            0,  # Joshuafurt
            0,  # Julieberg
            0,  # Justinport
            0,  # Lake Carolyn
            0,  # Lake Christinaport
            0,  # Lake Dariusborough
            0,  # Lake Jack
            0,  # Lake Jennifer
            0,  # Leahview
            0,  # Lewishaven
            0,  # Martinezfort
            0,  # Morrisport
            0,  # New Michele
            0,  # New Robinton
            0,  # North Erinville
            0,  # Port Adamtown
            0,  # Port Andrealand
            0,  # Port Daniel
            0,  # Port Jonathanborough
            0,  # Richardport
            0,  # Rickytown
            0,  # Scottberg
            0,  # South Anthony
            0,  # South Stevenfurt
            0,  # Toddshire
            0,  # Wendybury
            0,  # West Ann
            0,  # West Brittanyview
            0,  # West Gerald
            0,  # West Gregoryview
            0,  # West Lydia
            0  # West Terrence
        ]
        # scikit-learn assumes you want to predict the values for lots of houses at once, so it expects an array.
        # We just want to look at a single house, so it will be the only item in our array.
        homes_to_value = [
            house_to_value
        ]
        # return render_template("index.html", pred=house_to_value)
        # Run the model and make a prediction for each house in the homes_to_value array
        predicted_home_values = model.predict(homes_to_value)
        # Since we are only predicting the price of one house, just look at the first prediction returned
        predicted_value = predicted_home_values[0]
        return render_template("predict.html", pred=predicted_value)


def get_document_form(product_slug):
    if product_slug == 'dmca-complaint-takedown-notice':
        return DmcaComplaint()
    if product_slug == 'nepotism-policy':
        return NepotismPolicy()
    if product_slug == 'workplace-searches-policy':
        return WorkplaceSearchesPolicy()
    else:
        return DefaultProductRegistration()


def translate(title, text, language):
    parts = []
    # text = json.loads(text)
    parts.append(title)
    print(len(text))
    while len(text) > 5000:
        print(len(text))
        temp = text[0:5000]
        parts.append(temp)
        text = text[5000:]
        # parts.append(text[5000:])
    else:
        parts.append(text)
    translator = Translator(service_urls=[
        'translate.google.com',
        'translate.google.co.kr',
    ])
    translations = translator.translate(parts, dest=language)
    translated = ''
    for translation in translations:
        translated = translated + " " + translation.text
    translation = translated
    return translation


def get_document_statistics(text):
    source_text = text
    view_sentence_range = (0, 10)
    stats = []
    stats.append('Number of unique words: {}'.format(len({word: None for word in source_text.split()})))
    sentences = source_text.split('\n')
    word_counts = [len(sentence.split()) for sentence in sentences]
    stats.append('Number of sentences: {}'.format(len(sentences)))
    stats.append('Average number of words in a sentence: {}'.format(np.average(word_counts)))
    return stats
