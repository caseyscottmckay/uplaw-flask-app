from flask import current_app



def add_to_index(index, model):
    if not current_app.elasticsearch:
        return
    payload = {}
    for field in model.__searchable__:
        payload[field] = getattr(model, field)
    current_app.elasticsearch.index(index=index, doc_type=index, id=model.id,
                                    body=payload)


def remove_from_index(index, model):
    if not current_app.elasticsearch:
        return
    current_app.elasticsearch.delete(index=index, doc_type=index, id=model.id)


def query_index(index, query, page, per_page):

    if not current_app.elasticsearch:
        return [], 0
    '''
    search = current_app.elasticsearch.search(index='document', body={"query": {
        "bool": {
            "must": {
                "bool": {
                    "should": [
                        {"match": {"body.text": query}},
                        {"match": {"body.title": query}}
                    ]
                }
            },


        "must_not": [
            {"match": {"type": "opinion"}}
        ]
        }
    },
        'aggregations': {'types': {'terms': {'field': 'body.type.keyword'}},
                         'jurisdictions': {'terms': {'field': 'body.jurisdiction.keyword'}},
                         'courts': {'terms': {'field': 'body.court.keyword'}},
                         'dates': {'terms': {'field': 'body.date_created'}}},
        "highlight": {"number_of_fragments": 3, "fragment_size": 150,
                      "fields": {"_all": {"pre_tags": ["<em>"], "post_tags": ["</em>"]},
                                 "body.html": {"number_of_fragments": 5, "order": "score"}}},
        'from': (page - 1) * per_page, 'size': per_page})
    #ids = [(hit['_id']) for hit in search['hits']['hits']]
   
    hits = search['hits']['hits']
    aggregations = search['aggregations']
#    return hits, search['hits']['total'], aggregations

    '''
