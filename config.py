import os
from dotenv import load_dotenv
from flask import current_app

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'you-will-never-guess'
    #SQLALCHEMY_DATABASE_URI = 'mysql://casey:FONZO182csm!@localhost/uplaw'
    SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@localhost/uplaw'
    #SQLALCHEMY_DATABASE_URI = 'postgresql://postgres:postgres@35.245.116.199/uplaw'
    #SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
    #    'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    LOG_TO_STDOUT = os.environ.get('LOG_TO_STDOUT')
    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    ADMINS = ['contact@uplaw.us']
    LANGUAGES = ['en', 'es']
    MS_TRANSLATOR_KEY = os.environ.get('MS_TRANSLATOR_KEY')
    #ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
    ELASTICSEARCH_URL='http://35.184.16.52:9200'
    #ELASTICSEARCH_URL ='http://localhost:9200'
    REDIS_URL = os.environ.get('REDIS_URL') or 'redis://'
    POSTS_PER_PAGE = 25
    FLASK_APP = 'uplaw.py'
    #PUBLIC_DOWNLOAD_FOLDER = 'static/public/download/'
    PUBLIC_DOWNLOAD_FOLDER = 'static/public/download/'
    DOCX_TEMPLATE_FOLDER = 'templates/docx/'