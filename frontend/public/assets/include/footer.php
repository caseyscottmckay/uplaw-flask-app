
<!-- BEGIN: LAYOUT/FOOTERS/FOOTER-10 -->
<a name="footer"></a>
<footer class="c-layout-footer c-layout-footer-10 c-bg-white">
    <div class="c-footer">
        <div class="c-layout-footer-10-content container">
            <div class="row">
                <div class="col-md-5">
                    <div class="c-layout-footer-10-title-container">
                        <h3 class="c-layout-footer-10-title">About UpLaw</h3>
                        <div class="c-layout-footer-10-title-line"><span class="c-theme-bg"></span></div>
                    </div>
                    <p class="c-layout-footer-10-desc">
                        UpLaw allows you to look up the la by providing a global law library, smart search engine, and data analytics on over 10 million legal documents from 300+ countries.
                    </p>
                </div>
                <div class="col-md-7">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="c-layout-footer-10-title-container">
                                <h3 class="c-layout-footer-10-title">About Us</h3>
                                <div class="c-layout-footer-10-title-line"><span class="c-theme-bg"> </span></div>
                            </div>
                            <ul class="c-layout-footer-10-list">
                                <li class="c-layout-footer-10-list-item"><a href="#">Contact Us</a></li>
                                <li class="c-layout-footer-10-list-item"><a href="#">Branches</a></li>
                                <li class="c-layout-footer-10-list-item"><a href="#">Our Blog</a></li>
                                <li class="c-layout-footer-10-list-item"><a href="#">Careers</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <div class="c-layout-footer-10-title-container">
                                <h3 class="c-layout-footer-10-title">Services</h3>
                                <div class="c-layout-footer-10-title-line"><span class="c-theme-bg"> </span></div>
                            </div>
                            <ul class="c-layout-footer-10-list">
                                <li class="c-layout-footer-10-list-item"><a href="#">Advisory</a></li>
                                <li class="c-layout-footer-10-list-item"><a href="#">Institute</a></li>
                                <li class="c-layout-footer-10-list-item"><a href="#">Strategy</a></li>
                                <li class="c-layout-footer-10-list-item"><a href="#">Alliances</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <div class="c-layout-footer-10-title-container">
                                <h3 class="c-layout-footer-10-title">Partners</h3>
                                <div class="c-layout-footer-10-title-line"><span class="c-theme-bg"></span></div>
                            </div>
                            <ul class="c-layout-footer-10-list">
                                <li class="c-layout-footer-10-list-item"><a href="#">Clients</a></li>
                                <li class="c-layout-footer-10-list-item"><a href="#">Suppliers</a></li>
                                <li class="c-layout-footer-10-list-item"><a href="#">Investors</a></li>
                                <li class="c-layout-footer-10-list-item"><a href="#">Groups</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <div class="c-layout-footer-10-title-container">
                                <h3 class="c-layout-footer-10-title">Achievements</h3>
                                <div class="c-layout-footer-10-title-line"><span class="c-theme-bg"></span></div>
                            </div>
                            <ul class="c-layout-footer-10-list">
                                <li class="c-layout-footer-10-list-item"><a href="#">Awards</a></li>
                                <li class="c-layout-footer-10-list-item"><a href="#">Trophies</a></li>
                                <li class="c-layout-footer-10-list-item"><a href="#">Our Patents</a></li>
                                <li class="c-layout-footer-10-list-item"><a href="#">Key People</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="c-layout-footer-10-subfooter-container c-bg-grey">
            <div class="c-layout-footer-10-subfooter container">
                <div class="c-layout-footer-10-subfooter-content">
                    Copyright © 2017 UpLaw Theme. All Rights Reserved. No claim to government works.
                </div>
                <div class="c-layout-footer-10-subfooter-social">
                    <ul>
                        <li><a href="#" class="socicon-btn socicon-twitter tooltips" data-original-title="Twitter"></a></li>
                        <li><a href="#" class="socicon-btn socicon-facebook tooltips" data-original-title="Facebook"></a></li>
                        <li><a href="#" class="socicon-btn socicon-google tooltips" data-original-title="Google"></a></li>
                        <li><a href="#" class="socicon-btn socicon-yahoo tooltips" data-original-title="Yahoo"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer><!-- END: LAYOUT/FOOTERS/FOOTER-10 -->

<!-- BEGIN: LAYOUT/FOOTERS/GO2TOP -->
<div class="c-layout-go2top">
    <i class="icon-arrow-up"></i>
</div><!-- END: LAYOUT/FOOTERS/GO2TOP -->

<!-- BEGIN: LAYOUT/BASE/BOTTOM -->
<!-- BEGIN: CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="../../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="../../assets/plugins/jquery.min.js" type="text/javascript" ></script>
<script src="../../assets/plugins/jquery-migrate.min.js" type="text/javascript" ></script>
<script src="../../assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript" ></script>
<script src="../../assets/plugins/jquery.easing.min.js" type="text/javascript" ></script>
<script src="../../assets/plugins/reveal-animate/wow.js" type="text/javascript" ></script>
<script src="../../assets/demos/corporate_1/js/scripts/reveal-animate/reveal-animate.js" type="text/javascript" ></script>

<!-- END: CORE PLUGINS -->

<!-- BEGIN: LAYOUT PLUGINS -->
<script src="../../assets/plugins/revo-slider/js/jquery.themepunch.tools.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/revo-slider/js/jquery.themepunch.revolution.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/revo-slider/js/extensions/revolution.extension.slideanims.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/revo-slider/js/extensions/revolution.extension.layeranimation.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/revo-slider/js/extensions/revolution.extension.navigation.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/revo-slider/js/extensions/revolution.extension.video.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/cubeportfolio/js/jquery.cubeportfolio.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="../../assets/plugins/smooth-scroll/jquery.smooth-scroll.js" type="text/javascript"></script>
<script src="../../assets/plugins/typed/typed.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/slider-for-bootstrap/js/bootstrap-slider.js" type="text/javascript"></script>
<script src="../../assets/plugins/js-cookie/js.cookie.js" type="text/javascript"></script>
<!-- END: LAYOUT PLUGINS -->

<!-- BEGIN: THEME SCRIPTS -->
<script src="../../assets/base/js/components.js" type="text/javascript"></script>
<script src="../../assets/base/js/components-shop.js" type="text/javascript"></script>
<script src="../../assets/base/js/app.js" type="text/javascript"></script>
<script>
    $(document).ready(function() {
        App.init(); // init core
    });
</script>
<!-- END: THEME SCRIPTS -->

<!-- BEGIN: PAGE SCRIPTS -->
<script src="../../assets/demos/corporate_1/js/scripts/revo-slider/slider-15.js" type="text/javascript"></script>
<script src="../../assets/plugins/isotope/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/isotope/imagesloaded.pkgd.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/isotope/packery-mode.pkgd.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/ilightbox/js/jquery.requestAnimationFrame.js" type="text/javascript"></script>
<script src="../../assets/plugins/ilightbox/js/jquery.mousewheel.js" type="text/javascript"></script>
<script src="../../assets/plugins/ilightbox/js/ilightbox.packed.js" type="text/javascript"></script>
<script src="../../assets/demos/default/js/scripts/pages/isotope-gallery.js" type="text/javascript"></script>
<script src="../../assets/plugins/revo-slider/js/extensions/revolution.extension.parallax.min.js" type="text/javascript"></script>
<script src="../../assets/plugins/revo-slider/js/extensions/revolution.extension.kenburn.min.js" type="text/javascript"></script>
<!-- END: PAGE SCRIPTS -->
<!-- END: LAYOUT/BASE/BOTTOM -->
</body>
</html>