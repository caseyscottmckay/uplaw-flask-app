<?php
/**
 * Plugin Name: WooCommerce: Add Product to Cart by SKU
 * Plugin URI: http://remicorson.com
 * Description: Just a demo!
 * Version: 1.0
 * Author: Remi Corson
 * Author URI: http://remicorson.com/
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}



/**
 * WC Product Add to Cart by SKU class
 */
class WC_Add_to_Cart_by_SKU {

    /**
     * Constructor
     */
    public function __construct() {

        define( 'WC_ADD_TO_CART_BY_SKU_VERSION', '1.0' );
        define( 'WC_ADD_TO_CART_BY_SKU_PATH', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
        define( 'WC_ADD_TO_CART_BY_SKU_PLUGIN_URL', untrailingslashit( plugins_url( basename( plugin_dir_path( __FILE__ ) ), basename( __FILE__ ) ) ) );
    }

    /**
     * get_product_id_by_product_sku()
     *
     * Return product ID from product SKU
     */
    public function get_product_id_by_product_sku( $add_to_cart ) {

        global $wpdb;

        $product_id = $wpdb->get_var( $wpdb->prepare( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key='_sku' AND meta_value='%s' LIMIT 1", $add_to_cart ) );

        if ( $product_id ) return $product_id;

        return $add_to_cart;

    }

}

add_filter( 'woocommerce_add_to_cart_product_id', array( new WC_Add_to_Cart_by_SKU(), 'get_product_id_by_product_sku' ) );