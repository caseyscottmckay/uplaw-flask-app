<?php /* Template Name: Blog */
?>
<!-- Matomo -->
<script type="text/javascript">
    var _paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(["setCookieDomain", "*.app.uplaw.us"]);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u="//uplaw.us/analytics/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '2']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<noscript><p><img src="//uplaw.us/analytics/matomo.php?idsite=2&amp;rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Matomo Code -->
<?php wp_redirect( 'https://app.uplaw.us/blog', 301 ); exit; ?>

<?php include 'assets/include/header.php';?>
<!-- BEGIN: PAGE CONTAINER -->
<!-- BEGIN: Spacer/Buffer for header/menu -->
<div class="c-content-box c-size-lg c-theme-bg">
    <div class="container">
        <div class="c-content-title-1 c-opt-1 wow fadeInUp">


            <div class="c-line-center c-theme-bg"></div>
        </div>

    </div>
</div>
<!-- END: Spacer/Buffer for header/menu -->




	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">
		<!-- BEGIN: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->

<div class="c-layout-breadcrumbs-1 c-fonts-uppercase c-fonts-bold c-bordered c-bordered-both">
	<div class="container">
		<div class="c-page-title c-pull-left">
			<h3 class="c-font-uppercase c-font-sbold">BLawg</h3>
		</div>
		<ul class="c-page-breadcrumbs c-theme-nav c-pull-right c-fonts-regular">
															<li><a href="/">Home</a></li>
			<li>/</li>

															<li class="c-state_active">Blog</li>

		</ul>
	</div>
</div><!-- END: LAYOUT/BREADCRUMBS/BREADCRUMBS-1 -->
		<!-- BEGIN: PAGE CONTENT -->
		<!-- BEGIN: BLOG LISTING -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="c-content-blog-post-1-list">
				    <?php
                    $recent_posts = wp_get_recent_posts();
                        foreach( $recent_posts as $recent ){
                            echo '<div class="c-content-blog-post-1"><div class="c-title c-font-bold c-font-uppercase"><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </div>
                            <div class="c-desc">
							Lorem ipsum dolor sit amet, coectetuer diam ipsum dolor sit amet nonummy  coectetuer diam ipsum dolor sit
							coectetuer adipiscing elit adipiscing consectetuer ipsum dolor sit amipiscing elit sit amet, sit amet,
							coectetuer adipiscing elit adipiscing consectetuer ipsum dolor sit amet diam nonummy adipiscing elit sit amet, sit ame.
							<a href="#">read more...</a>
						    </div>
						    <div class="c-panel">
							    <div class="c-author"><a href="#">By <span class="c-font-uppercase">Nick Strong</span></a></div>
							    <div class="c-date">on <span class="c-font-uppercase">20 May 2015, 10:30AM</span></div>
							    <ul class="c-tags c-theme-ul-bg">
								    <li>hi-tech</li>
								    <li>enginering</li>
								    <li>robots</li>
							    </ul>
							    <div class="c-comments"><a href="#"><i class="icon-speech"></i> 30 comments</a></div>
						    </div>
						    </div>';


                        }
                        wp_reset_query();
                    ?>



					<div class="c-pagination">
						<ul class="c-content-pagination c-theme">
							<li class="c-prev"><a href="#"><i class="fa fa-angle-left"></i></a></li>
							<li><a href="#">1</a></li>
							<li class="c-active"><a href="#">2</a></li>
							<li><a href="#">3</a></li>
							<li><a href="#">4</a></li>
							<li class="c-next"><a href="#"><i class="fa fa-angle-right"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-3">
			<!-- BEGIN: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
<form action="#" method="post">
	<div class="input-group">
      <input type="text" class="form-control c-square c-theme-border" placeholder="Search blog...">
      <span class="input-group-btn">
        <button class="btn c-theme-btn c-theme-border c-btn-square" type="button">Go!</button>
      </span>
    </div>
</form>

<div class="c-content-ver-nav">
	<div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
		<h3 class="c-font-bold c-font-uppercase">Categories</h3>
		<div class="c-line-left c-theme-bg"></div>
	</div>
	<ul class="c-menu c-arrow-dot1 c-theme">
		<li><a href="#">Commercial(2)</a></li>
		<li><a href="#">Corporate(12)</a></li>
		<li><a href="#">IP & Technology(5)</a></li>
		<li><a href="#">Labor & Employment(7)</a></li>
		<li><a href="#">Litigation(11)</a></li>
		<li><a href="#">Real Estate(18)</a></li>
	</ul>
</div>

<div class="c-content-tab-1 c-theme c-margin-t-30">
	<div class="nav-justified">
		<ul class="nav nav-tabs nav-justified">
		    <li class="active"><a href="#blog_recent_posts" data-toggle="tab">Recent Posts</a></li>
		    <li><a href="#blog_popular_posts" data-toggle="tab">Popular Posts</a></li>
		</ul>
		<div class="tab-content">
	    	<div class="tab-pane active" id="blog_recent_posts">
	    		<ul class="c-content-recent-posts-1">
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/09.jpg" alt="" class="img-responsive">
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/08.jpg" alt="" class="img-responsive">
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/07.jpg" alt="" class="img-responsive">
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/32.jpg" alt="" class="img-responsive">
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    		</ul>
	    	</div>
	    	<div class="tab-pane" id="blog_popular_posts">
	    		<ul class="c-content-recent-posts-1">
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/34.jpg" class="img-responsive" alt=""/>
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/37.jpg" class="img-responsive" alt=""/>
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/32.jpg" class="img-responsive" alt=""/>
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    			<li>
	    				<div class="c-image">
	    					<img src="../../assets/base/img/content/stock/54.jpg" class="img-responsive" alt=""/>
	    				</div>
	    				<div class="c-post">
	    					<a href="" class="c-title">
	    					UX Design Expo 2015...
	    					</a>
	    					<div class="c-date">27 Jan 2015</div>
	    				</div>
	    			</li>
	    		</ul>
	    	</div>
	  	</div>
  	</div>
</div>

<div class="c-content-ver-nav">
	<div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
		<h3 class="c-font-bold c-font-uppercase">Blogs</h3>
		<div class="c-line-left c-theme-bg"></div>
	</div>
	<ul class="c-menu c-arrow-dot c-theme">
		<li><a href="#">Fasion & Arts</a></li>
		<li><a href="#">UX & Web Design</a></li>
		<li><a href="#">Mobile Development</a></li>
		<li><a href="#">Internet Marketing</a></li>
		<li><a href="#">Frontend Development</a></li>
	</ul>
</div><!-- END: CONTENT/BLOG/BLOG-SIDEBAR-1 -->
			</div>
		</div>
	</div>
</div>
<!-- END: BLOG LISTING  -->

		<!-- END: PAGE CONTENT -->
	</div>
	<!-- END: PAGE CONTAINER -->


    <?php include 'assets/include/footer.php';?>
