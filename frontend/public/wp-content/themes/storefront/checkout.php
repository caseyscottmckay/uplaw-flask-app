<?php /* Template Name: Checkout */


get_header();
//gravity_form_enqueue_scripts( 2, true );

?>

<p>An UpLaw Legal Representative will contact you to collect information necessary to complete your order. Most orders are complete and available for download in you <a href="my-account">account dashboard</a> within 3-5 business days; however, long, complex documents may take longer to process. If you have questions or want to complete your order by phone, call us at 929.322.3669 Mon-Fri 5am-7pm EST, Weekends 7am - 4pm EST or send us an email at support@uplaw.us.</p>
<?php echo do_shortcode('[woocommerce_checkout]'); ?>

