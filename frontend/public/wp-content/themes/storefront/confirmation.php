<?php /* Template Name: Confirmation */
?>
<!-- Matomo -->
<script type="text/javascript">
    var _paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(["setCookieDomain", "*.app.uplaw.us"]);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u="//uplaw.us/analytics/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '2']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<noscript><p><img src="//uplaw.us/analytics/matomo.php?idsite=2&amp;rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Matomo Code -->

<?php var_dump($_REQUEST);
    $entry_id=$_REQUEST['entry_id'];
    $slug = $_REQUEST['slug'];
 wp_redirect( "https://app.uplaw.us/confirmation?slug=$slug&entry_id=$entry_id", 301 ); exit; ?>