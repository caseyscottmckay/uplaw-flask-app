<?php /* Template Name: Consultation */
?>
<!-- Matomo -->
<script type="text/javascript">
    var _paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(["setCookieDomain", "*.app.uplaw.us"]);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u="//uplaw.us/analytics/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '2']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<noscript><p><img src="//uplaw.us/analytics/matomo.php?idsite=2&amp;rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Matomo Code -->

<?php
    //include 'assets/include/header.php';
    gravity_form_enqueue_scripts( 2, true );
    $parts = preg_split("#/#", $_REQUEST['product_url']);
    $slug = $parts[4];
    $product_obj = get_page_by_path( $slug, OBJECT, 'product' );
    $product_id = $product_obj->ID;
    $forms = GFFormsModel::get_forms();
    $form_exists=0;
	foreach($forms as &$form){
		    if ($form->title == $slug) {
		        $form_exists=1;
		    }
	    }
	    if($form_exists==0){
            $product_checkout_url="https://uplaw.us/checkout?add-to-cart=".$product_id;
            echo $product_checkout_url;
		    wp_redirect($product_checkout_url, 301 ); exit;
        }
    get_header();
?>
<!-- BEGIN: Spacer/Buffer for header/menu -->
<div class="c-content-box c-size-lg c-theme-bg">
    <div class="container">
        <div class="c-content-title-1 c-opt-1 wow fadeInUp">


            <div class="c-line-center c-theme-bg"></div>
        </div>

    </div>
</div>
<!-- END: Spacer/Buffer for header/menu -->




	<!-- BEGIN: PAGE CONTAINER -->
	<div class="c-layout-page">

		<!-- BEGIN: PAGE CONTENT -->
<div class="c-content-box c-size-md">
	<div class="container">
		<div class="row">
		<?php
			    echo $form_exists;

		gravity_form($slug, false, false, false, '', false);?>

		</div>
	</div>
</div>

<?php
    get_footer();
    //include 'assets/include/footer.php' ?>