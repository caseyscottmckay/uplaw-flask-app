<?php /* Template Name: Form */?>

<!-- Matomo -->
<script type="text/javascript">
    var _paq = window._paq || [];
    /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
    _paq.push(["setCookieDomain", "*.app.uplaw.us"]);
    _paq.push(['trackPageView']);
    _paq.push(['enableLinkTracking']);
    (function() {
        var u="//uplaw.us/analytics/";
        _paq.push(['setTrackerUrl', u+'matomo.php']);
        _paq.push(['setSiteId', '2']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
        g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
<noscript><p><img src="//uplaw.us/analytics/matomo.php?idsite=2&amp;rec=1" style="border:0;" alt="" /></p></noscript>
<!-- End Matomo Code -->


<?php //echo do_shortcode('[woocommerce_checkout]'); ?>
<?php
    //get_header();
    gravity_form_enqueue_scripts( 2, true );
    include 'assets/include/header.php';
?>

 <!-- BEGIN: Spacer/Buffer for header/menu -->
<div class="c-content-box c-size-lg c-theme-bg">
    <div class="container">
        <div class="c-content-title-1 c-opt-1 wow fadeInUp">


            <div class="c-line-center c-theme-bg"></div>
        </div>

    </div>
</div>
<div class="c-content-box c-size-lg c-bg-white">
    <div class="container">
        <div class="c-content-title-1 c-opt-1 wow fadeInUp">

            <div class="c-line-center c-theme-bg"></div>
        </div>

    </div>
    <div class="row">
    <?php gravity_form('background-check-policy', false, false, false, '', false); ?>

    </div>
</div>

<?php include 'assets/include/footer.php';?>

