from app import create_app, db, cli
from app.models import Document, User, Post, Message, Notification, Task

app = create_app()
cli.register(app)


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'Document': Document, 'User': User, 'Post': Post, 'Message': Message, 'Notification': Notification, 'Task': Task}
